<?php
use Chrisbjr\ApiGuard\ApiGuardController;
/**
*@package API
*
*/
class ApiController extends ApiGuardController {

		protected $apiMethods = [
        'search' => [
            'keyAuthentication' => true
	        ],
	    ];


    /**
    *@api
    *Use GET method to access this route
    *@link http://timemachine.truthmeter.mk/api/v1/search
    *@author Brilant Kasami
    *@param q (search term)
    *@param lang ('sq', 'mk', 'en') default en
    *@param sources OPTIONAL (source_name = id) example Alsat+M=13 @see http://timemachine.truthmeter.mk/api/v1/getSources
    *@param sm OPTIONAL (when searching multiple keywords add sm=1 to use OR) ommit this to use AND in the query
    *@param X-Authorization (Api key)
    *example http://timemachine.truthmeter.mk/api/v1/search?X-Authorization=418e701c797a96b7fd7d3c2b3458555d2d2473dd&lang=sq&q=vmro
    *example2 http://timemachine.truthmeter.mk/api/v1/search?X-Authorization=418e701c797a96b7fd7d3c2b3458555d2d2473dd&lang=sq&q=vmro&Alsat+M=13&TV+Koha=12&sm=1
    *@return json objects
    **/
    public function search()
    {
        $term = Input::get('q');
        $searchTerms = explode(" ", $term);


        //added for api
        $lang = Input::get('lang', 'en');
        $limit = (int)Input::get('limit', 10);
        $offset = (int)Input::get('offset', 0);
        if($offset - $limit > 100)
                return Response::json('Invalid offset/limit');
        //end api added
        $sources = array();

        $sor = Input::all();
        unset($sor['page']);
        unset($sor['trends']);
        unset($sor['sr_tr']);
        unset($sor['sm']);
        unset($sor['limit']);
        unset($sor['offset']);
        foreach($sor as $a)
        {//selected a source get by id
                    if(is_numeric($a))
                        $sources[] = (int)$a;
        }

        if(sizeof($sources)==0){ //selected all
            $src = Sources::where('source_language', '=',Str::upper($lang))->remember(100)->get();
            foreach($src as $s){
                $sources[] = $s->id;
            }

        }
        //has term
        if(Input::has('trends')){
                $rs = array();

                if(!Input::has('q')){//no term get last ten keywords
                     /*$r = DB::table('news_terms')
                     ->select('term', DB::raw("count(*) AS tc"))
                     ->groupBy('term')
                     ->orderBy('tc', 'desc')
                     ->take(10)->lists('term');*/
                     $searchTerms = Helpers::getLastTenTrends();
                }

                foreach($searchTerms as $st){
                    //dd($st);
                      $r = DB::table('news_terms')
                     ->join('news_articles', 'news_terms.news_article_id', '=', 'news_articles.id')
                     ->select(DB::raw("date_trunc('day', retrieved_time) AS day, extract(epoch from date_trunc('day', news_articles.retrieved_time)) as dt, count(*) AS tc"))
                     ->where('term', '=', $st)
                     ->whereIn('news_terms.source_id',  $sources)
                     ->groupBy('day')
                     ->orderBy('day')
                     ->get();
                     $r['q'] = $st;
                     $rs[] = $r;
                }
                 if(Input::has('sr_tr'))
                        return Response::json($rs, 200);
                 return View::make('search_trends', array('searchResult'=> $rs));
        }
        else{
            //has not term

            $r = NewsArticles::whereNull('parent_id');

            if(!Input::has('q') || strcmp($term, "*") == 0){//no term get last ten keywords
                     /*$r = DB::table('news_terms')
                     ->select('term', DB::raw("count(*) AS tc"))
                     ->groupBy('term')
                     ->orderBy('tc', 'desc')
                     ->take(10)->lists('term');*/
                     //$searchTerms = Helpers::getLastTenTrends();
                     //$r->orderBy('id', 'DESC');
             } else{
                //DECIDE to use 'OR' or 'AND' if input::has('sm') then use OR else use AND
                            if(Input::has('sm')){
                                $r->WhereHas('nt', function($query) use ($searchTerms)
                                {
                                            //$query->where('term', '=', $term);
                                            $query->whereIn('term',$searchTerms);

                                });
                            }
                            else{
                                
                                foreach($searchTerms as $s){
                                    $r->whereHas('nt', function($query) use ($s)
                                    {
                                                $query->where('term', '=', $s);
                                    });
                                }
                            }

             }

            

            $r->whereIn('source_id',  $sources);
            if(Input::has('from') && Input::has('to')){
                    $r->where('retrieved_time', '>', Carbon\Carbon::parse(Input::get('from'))->startOfDay())
                    ->where('retrieved_time', '<=', Carbon\Carbon::parse(Input::get('to'))->endOfDay());
            }
            //$r->whereNull('parent_id');
            $r->orderBy('id', 'DESC');
            $r->orderBy('rss_time', 'DESC');
            $r->take($limit);
            $r->offset($offset);
            $r = $r->get();

            foreach($r as $a){
                $similar = [];
                foreach($a->similar as $s){
                    //print_r($s);
                    $sim = [];
                    $similar[] = $s->article->id;
                }
                $a["similar"] = $similar;
                //print_r($a);
                
            }
            //dd("haha");

            //dd($r);
                $response = Response::make($r, 200);

                $response->header('Access-Control-Allow-Origin', '*');
                return $response;
            }
    }

    /**
    *@api
    *Use GET method to access this route
    *@link http://timemachine.truthmeter.mk/api/v1/getSources
    *@author Brilant Kasami
    *@param lang ('sq', 'mk', 'en') default en
    *@param X-Authorization (Api key)
    *example http://timemachine.truthmeter.mk/api/v1/getSources?X-Authorization=418e701c797a96b7fd7d3c2b3458555d2d2473dd&lang=sq
    *@return json objects
    **/
    public function getSources(){
        $lang = Input::get('lang', 'en');
        $lang = Str::upper($lang);
        $sources = Sources::where('source_language' , '=', $lang)->orderBy('name')->get();
        $response = Response::make($sources, 200);

        $response->header('Access-Control-Allow-Origin', '*');
        return $response;

    }
}