<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return View::make('hello');
	}

	public function insertUrl(){

			$insert_url = Input::get('insert_url');
			$date = Input::get('date');
			$rules =  array('captcha' => array('required', 'captcha'));
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails())
            {
                return View::make('home', array('news_inserted_failure' => 'captcha error', 'insert_url' => $insert_url));
            }
            else
            {
           	    $output = array();
		        $return_var = -1;
		        
		        $path = '/var/www/html/news/vendor/bin/crawl_single_page.sh "'.$insert_url.'" "'.$date.'"';
		        $r = exec('/var/www/html/news/vendor/bin/crawl_single_page.sh "'.$insert_url.'" '.$date, $output, $return_var);
		        $l = strlen($r);
		        if($l >0){ //sucess inserted url to crawler
		                $a =  Helpers::checkUrlExistence($insert_url);
		                if($a)
		                    return Redirect::to('/'.$a->id);
		        }else{//failed
		                return View::make('home', array('news_inserted_failure' => 'crawler failure', 'insert_url' => $insert_url));
		        }
            }
            return View::make('home', array('news_inserted_failure' => 'Unknown error', 'insert_url' => $insert_url));
	}
	public function doSearch(){

		$term = Input::get('q');
		$searchTerms = explode(" ", $term);

		$sources = array();

		$sor = Input::all();
		unset($sor['page']);
		unset($sor['trends']);
		unset($sor['sr_tr']);
		unset($sor['sm']);
		foreach($sor as $a)
		{//selected a source get by id
					if(is_numeric($a))
						$sources[] = (int)$a;
		}

		if(sizeof($sources)==0){ //selected all
			$src = Sources::where('source_language', '=',Str::upper(App::getLocale()))->remember(100)->get();
			foreach($src as $s){
				$sources[] = $s->id;
			}

		}
		//has term
		if(Input::has('trends')){
				$rs = array();

				if(!Input::has('q')){//no term get last ten keywords
					 /*$r = DB::table('news_terms')
                     ->select('term', DB::raw("count(*) AS tc"))
                     ->groupBy('term')
                     ->orderBy('tc', 'desc')
                     ->take(10)->lists('term');*/
                     $searchTerms = Helpers::getLastTenTrends();
				}

				foreach($searchTerms as $st){
					//dd($st);
				      $r = DB::table('news_terms')
           			 ->join('news_articles', 'news_terms.news_article_id', '=', 'news_articles.id')
                     ->select(DB::raw("date_trunc('day', retrieved_time) AS day, extract(epoch from date_trunc('day', news_articles.retrieved_time)) as dt, count(*) AS tc"))
                     ->where('term', '=', $st)
                     ->whereIn('news_terms.source_id',  $sources)
                     ->groupBy('day')
                     ->orderBy('day')
                     ->get();
                     $r['q'] = $st;
                     $rs[] = $r;
				}
                 if(Input::has('sr_tr'))
                 		return Response::json($rs, 200);
                 return View::make('search_trends', array('searchResult'=> $rs));
		}
		else{
			//has not term

			$r = NewsArticles::whereNull('parent_id');

			//DECIDE to use 'OR' or 'AND' if input::has('sm') then use OR else use AND
			if(Input::has('sm')){
				$r->WhereHas('nt', function($query) use ($searchTerms)
				{
						    //$query->where('term', '=', $term);
							$query->whereIn('term',$searchTerms);

				});
			}
			else{
				
				foreach($searchTerms as $s){
					$r->whereHas('nt', function($query) use ($s)
					{
								$query->where('term', '=', $s);
					});
				}
			}

			$r->whereIn('source_id',  $sources);
			if(Input::has('from') && Input::has('to')){
					$r->where('retrieved_time', '>', Carbon\Carbon::parse(Input::get('from'))->startOfDay())
					->where('retrieved_time', '<=', Carbon\Carbon::parse(Input::get('to'))->endOfDay());
			}
			//$r->whereNull('parent_id');
			$r->orderBy('id', 'DESC');
			$r->orderBy('rss_time', 'DESC');
			$r = $r->paginate(10);

			//dd($r);
			return View::make('search', array('searchResult'=> $r));
			}
	}

}
