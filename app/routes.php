<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


//

// Confide routes
Route::get('admin/page/administrator.signup', 'UsersController@create');
Route::post('users', 'UsersController@store');
Route::get('login', 'UsersController@login');
Route::post('login', 'UsersController@doLogin');
Route::get('confirm/{code}', 'UsersController@confirm');
Route::get('forgot_password', 'UsersController@forgotPassword');
Route::post('users/forgot_password', 'UsersController@doForgotPassword');
Route::get('users/reset_password/{token}', 'UsersController@resetPassword');
Route::post('users/reset_password', 'UsersController@doResetPassword');
Route::get('logout', 'UsersController@logout');
Route::get('/api', function(){
    return View::make('apidoc');
});
Route::get('/api/v1/search', 'ApiController@search');
Route::get('/api/v1/getSources', 'ApiController@getSources');
Route::post('/admin/page/administrator.dashboards', array('as' => 'postdashboard', function(){
    //dd(Input::get('from'));
    return Redirect::to('/admin/page/administrator.dashboards')->with(array('from'=> Input::get('from'), 'to' => Input::get('to')));
}));

Route::get('/test', function(){
    return "1,2,3";
});
Route::group(array(
    'prefix' => LaravelLocalization::setLocale(), 
    'before' => 'setLangFromDomain'
    ), 
    function()
    {
        /** ADD ALL LOCALIZED ROUTES INSIDE THIS GROUP **/
        Route::get('/', function()
        {
            //dd(App::getLocale());
            //dd(URL::to('/'));
            

            return View::make('home');
        });
        
        Route::get('/{id}/{embed?}', function($id = null, $embed = null){
            if($embed)
                return View::make('news_details_embed', array('id'=> $id));
        	return View::make('news_details', array('id'=> $id));
		})->where('id', '[0-9]+');

        Route::get('/za-proektot', function($id = null, $embed = null){
                return View::make('about.za-proektot');
        });

        Route::get('/per-projektin', function($id = null, $embed = null){
                return View::make('about.per-projektin');
        });

        Route::get('/about-project', function($id = null, $embed = null){
                return View::make('about.about-project');
        });

        Route::get('/about-us', function($id = null, $embed = null){
                return View::make('about.about-us');
        });


        Route::get('search', array('as' => 'getsearch' ,'uses' => 'HomeController@doSearch'));
        Route::post('/inserturl', array('as' => 'inserturl' ,'uses' => 'HomeController@insertUrl'));
        Route::post('/search', array('as' => 'postsearch', function($term = null){
             $searchQuery = Input::get('searchQuery'); 
             $sources = Input::get('source');
             $sourceList = explode(",", $sources);


            //begin url validator

                $validator = Validator::make(
                array('q' => $searchQuery),
                array('q' => array('required', 'url'))
                            );

                        if ($validator->passes())
                        {
                            //dd($searchQuery);
                            $a =  Helpers::checkUrlExistence($searchQuery);
                            //dd($a);
                            //return Redirect::to('/')->with('popupOpen' => true);
                            if($a){
                                return Redirect::to('/'.$a->id);
                            }else{
                                return View::make('home', array('popupOpen' => true, 'insert_url' => $searchQuery));
                            }
                        }
                       /* else
                        {
                            //dd("failled");
                        }*/

            //end url validator

             //dd($sourceList);

             $arr = array();
             if(!empty($searchQuery)){
                $arr['q'] = mb_convert_case($searchQuery, MB_CASE_LOWER, "UTF-8");
             }
             
             if(sizeof($sourceList)>0)
             foreach($sourceList as $l){
                //dd($sourceList);
                $id = Sources::where('name', '=', $l)->remember(1)->first();
                if($id)
                    $arr[$l] = $id->id;
             }
             if(in_array('all', $sourceList)){
                //dd($sourceList);
                $arr['all'] = 'all';
             }

             if(Input::has('from') && Input::has('to')){
                    $arr['from'] = Input::get('from');
                    $arr['to'] = Input::get('to');
             }

             if(Input::has('trnds')){
                $arr['trends'] = 1;
             }
             else
             {
                unset($arr['trends']);
             }

             //Shall we use 'OR' in where query or 'AND'
             if(Input::has('srchmethod')){
                $arr['sm'] = 1;
             }
             else
             {
                unset($arr['sm']);
             }

             $a = route('getsearch', $arr );
             return Redirect::to($a)->withInput();
        }));
    });