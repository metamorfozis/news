<?php
class Settings extends Eloquent {
    protected $table = 'settings';

     public static function boot()
    {
        parent::boot();

        static::saved(function($post)
        {
            Cache::flush();
            //dd('aaaaaaa');
        });

        static::updated(function($post)
        {
            Cache::flush();
            //dd('aaa');
        });
    }

}