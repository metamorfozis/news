<?php

use Zizaco\Confide\ConfideUser;
use Zizaco\Confide\ConfideUserInterface;
use Zizaco\Entrust\HasRole;

class User extends Eloquent implements ConfideUserInterface
{
    use ConfideUser;
	use HasRole;

	public function assignedrole()
	{
	    return $this->belongsTo('AssignedRole', 'id', 'user_id');
	}

	public function setAssignRoleAttribute($role) { 
		//dd($this->assignedrole());
	    if(empty($role))
	        return false;
	    //dd($role);
	    //$this->models()->update(['manufacturer_id' => Manufacturer::where('name', $manufacturer)->first()->id]);

	}
}