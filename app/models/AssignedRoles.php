<?php

class AssignedRole extends Eloquent{
	protected $table = "assigned_roles";

	public function role()
	{
	    return $this->belongsTo('Role', 'role_id', 'id');
	}

	public function user()
	{
	    return $this->belongsTo('User', 'user_id', 'id');
	}
}