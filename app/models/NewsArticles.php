<?php
class NewsArticles extends Eloquent {
    protected $table = 'news_articles';

    public function source(){
    	return $this->belongsTo('Sources', 'source_id', 'id');
    }

    public function nt(){
    	return $this->belongsTo('NewsTerms', 'id', 'news_article_id');
    }

    function similar()
    {
        return $this->hasMany('Similarity', 'news1_id', 'id')->where('similarity.percentage', '>', Helpers::getSettingsCache('similarity_threshhold'))->orderBy('percentage', 'desc')->take(3);
    }

    //similar only takes 3, use this relation inside news details page to list all possible similarities
    function similarall()
    {
        return $this->hasMany('Similarity', 'news1_id', 'id')->where('similarity.percentage', '>', Helpers::getSettingsCache('similarity_threshhold'))->orderBy('percentage', 'desc');
    }
}