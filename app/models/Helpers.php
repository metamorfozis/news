<?php

class Helpers{
	public static function getSettingsCache($val){
		$value = Cache::rememberForever('settings', function()
		{
		    return Settings::orderBy('id', 'desc')->first();
		});
		return $value->$val;
	}

	public static function formatText($text){
			if(self::getSettingsCache('full_content'))
				return $text;
			else
				return str_limit($text, $limit = self::getSettingsCache('short_content_chars'), $end = '...');
	}

	public static function getLastTenTrends(){
		$r = DB::table('news_terms')
                     ->select('term', DB::raw("count(*) AS tc"))
                     ->groupBy('term')
                     ->orderBy('tc', 'desc')
         ->take(10)->lists('term');
         return $r;
	}

	public static function checkUrlExistence($url){
		return NewsArticles::where('url', '=', $url)->first();
		//return ($count >0) ? true : false;
	}

	public static function rssTimeOrRetrievedTime($rss_time, $retrieved_time){
		if($rss_time)
			return Carbon\Carbon::parse($rss_time)->format('d-m-Y H:i:s');
		return Carbon\Carbon::parse($retrieved_time)->tz('Europe/Skopje')->format('d-m-Y H:i:s');
	}
	public static function rssTimeOrRetrievedTimeDt($rss_time, $retrieved_time){
		if($rss_time)
			return Carbon\Carbon::parse($rss_time);
		return Carbon\Carbon::parse($retrieved_time)->tz('Europe/Skopje');
	}
	public static function checkImgExistence($imgName, $id){
		//img/no_image.png for no images
		if(strcmp($imgName, "/img/no_image.png") != 0){
			return $imgName;
		}
		return asset('images/screenshots/cache/'.$id.'.jpg');
	}
}