<?php
class Rule extends Eloquent {
    protected $table = 'rules';

     public function source()
    {
        return $this->belongsTo('Sources', 'source_id', 'id');
    }
}