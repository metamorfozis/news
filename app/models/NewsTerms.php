<?php
class NewsTerms extends Eloquent {
    protected $table = 'news_terms';

    public function source(){
    	return $this->belongsTo('Sources', 'source_id', 'id');
    }

    public function news(){
    	return $this->belongsTo('NewsArticles', 'news_article_id', 'id');
    }

    public function newsts(){
    	return $this->belongsTo('NewsArticles', 'news_article_id', 'id')->select(DB::raw("id, extract(epoch from date_trunc('day', retrieved_time)) as dt"));
    }
}