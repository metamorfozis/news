<?php

/**
 * Actors model config
 */

return array(

	'title' => 'Settings',

	'single' => 'Setting',

	'model' => 'Settings',

	/**
	 * The display columns
	 */
	'columns' => array(
		'id',
		'full_content' => array(
		    'title' => 'Full Content',
			'sort_field' => 'full_content',
		),
		'short_content_chars'=> array(
		    'title' => 'Short Content chars',
			'sort_field' => 'short_content_chars',
		),
		'show_screenshot' => array(
			'title' => 'Show Screenshot',
			'sort_field' => 'show_screenshot',
		),
		'similarity_threshhold' => array(
			'title' => 'Similarity Threshold',
			'sort_field' => 'similarity_threshhold',
		),
		'keyword_min_apperance' => array(
			'title' => 'Keyword Main Apperance',
			'sort_field' => 'keyword_min_apperance',
		)
	),

	/**
	 * The filter set
	 */
	'filters' => array(
		'id',
		'full_content' => array(
		    'title' => 'Full Conent',
		),
		'short_content_chars' => array(
		    'title' => 'Short Conent Chars',
		),
		'show_screenshot' => array(
			'title' => 'show_screenshot',
		),
		'similarity_threshhold' => array(
			'title' => 'similarity_threshhold',
		),
		'keyword_min_apperance' => array(
			'title' => 'keyword_min_apperance',
		)
	),

	/**
	 * The editable fields
	 */
	'edit_fields' => array(
		'full_content' => array(
			'type' => 'bool',
		    'title' => 'Full Content',
		),
		'short_content_chars' => array(
			'type' => 'text',
		    'title' => 'Short Content Chars',
		),
		'show_screenshot' => array(
			'title' => 'Show Screenshot',
			'type' => 'bool',
		),
		'similarity_threshhold' => array(
			'title' => 'Similarity Threshold',
			'type' => 'text',
		),
		'keyword_min_apperance' => array(
			'title' => 'Keyword Min Apperance',
			'type' => 'text',
		)
	),

	'rules' => array(
    	'full_content' 			=> 'required|boolean',
    	'short_content_chars' 	=> 'required|numeric|between:100,1000',
    	'show_screenshot'	 	=> 'required|boolean',
    	'similarity_threshhold' => 'required|numeric|between:0,1',
    	'keyword_min_apperance' => 'required|numeric|between:0,1000'
	),

	'action_permissions'=> array(
			'create' => false,
			'delete'=>false,
	),

);