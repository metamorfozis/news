<?php

/**
 * Actors model config
 */

return array(

	'title' => 'Rules',

	'single' => 'Rule',

	'model' => 'Rule',

	/**
	 * The display columns
	 */
	'columns' => array(
		'id',
		'source_name' => array(
		    'title' => 'source name',
		    'relationship' => 'source',
		    'select' => "(:table).name"
		),
		'tittle_rule' => array(
			'title' => 'tittle_rule',
			'sort_field' => 'tittle_rule',
		),
		'content_rule' => array(
			'title' => 'content_rule',
			'sort_field' => 'content_rule',
		),
		'media_rule' => array(
			'title' => 'media_rule',
			'sort_field' => 'media_rule',
		),
		'author_rule' => array(
			'title' => 'author_rule',
			'sort_field' => 'author_rule',
		),
		'date_rule' => array(
			'title' => 'date_rule',
			'sort_field' => 'date_rule',
		)
	),

	/**
	 * The filter set
	 */
	'filters' => array(
		'id',
		'source' => array(
			'type' => 'relationship',
		    'title' => 'source',
		    'name_field' => 'name', //what column or accessor on the other table you want to use to 
		),
		'tittle_rule' => array(
			'title' => 'tittle_rule',
		),
		'content_rule' => array(
			'title' => 'content_rule',
		),
		'media_rule' => array(
			'title' => 'media_rule',
		),
		'author_rule' => array(
			'title' => 'author_rule',
		),
		'date_rule' => array(
			'title' => 'date_rule',
		)
	),

	/**
	 * The editable fields
	 */
	'edit_fields' => array(
		'source' => array(
			'type' => 'relationship',
		    'title' => 'source',
		    'name_field' => 'name', //what column or accessor on the other table you want to use to 
		),
		'tittle_rule' => array(
			'title' => 'tittle_rule',
			'type' => 'text',
		),
		'content_rule' => array(
			'title' => 'content_rule',
			'type' => 'text',
		),
		'media_rule' => array(
			'title' => 'media_rule',
			'type' => 'text',
		),
		'author_rule' => array(
			'title' => 'author_rule',
			'type' => 'text',
		),
		'date_rule' => array(
			'title' => 'date_rule',
			'type' => 'text',
		)
	),

	'rules' => array(
    	'source_id' => 'required',
    	'tittle_rule' => 'required',
    	'content_rule' => 'required'
	),

);