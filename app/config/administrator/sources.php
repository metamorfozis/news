<?php

/**
 * Actors model config
 */

return array(

	'title' => 'Sources',

	'single' => 'Source',

	'model' => 'Sources',

	/**
	 * The display columns
	 */
	'columns' => array(
		'id',
		'name' => array(
			'title' => 'name',
			'sort_field' => 'name',
		),
		'rss_feed' => array(
			'title' => 'rss_feed',
			'sort_field' => 'rss_feed',
		),
		'homepage' => array(
			'title' => 'homepage',
			'sort_field' => 'homepage',
		),
		'source_language' => array(
			'title' => 'source_language',
			'sort_field' => 'source_language',
		)
	),

	/**
	 * The filter set
	 */
	'filters' => array(
		'id',
		'name' => array(
			'title' => 'name',
		),
		'rss_feed' => array(
			'title' => 'rss_feed',
		),
		'homepage' => array(
			'title' => 'homepage',
		),
		'source_language' => array(
			'title' => 'source_language',
		)
	),

	/**
	 * The editable fields
	 */
	'edit_fields' => array(
		'name' => array(
			'title' => 'name',
			'type' => 'text',
		),
		'rss_feed' => array(
			'title' => 'rss_feed',
			'type' => 'text',
		),
		'homepage' => array(
			'title' => 'homepage',
			'type' => 'text',
		),
		'source_language' => array(
			'title' => 'source_language',
			'type' => 'text',
		)
	),
	'rules' => array(
    	'name' => 'required',
    	'homepage' => 'required',
    	'source_language' => 'required'
	),

);