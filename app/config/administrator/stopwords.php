<?php

/**
 * Actors model config
 */

return array(

	'title' => 'Stop Words',

	'single' => 'Stop Word',

	'model' => 'Stopwords',

	/**
	 * The display columns
	 */
	'columns' => array(
		'id',
		'word' => array(
		    'title' => 'Word',
			'sort_field' => 'word',
		),
		'language' => array(
			'title' => 'Language',
			'sort_field' => 'language',
		)
	),

	/**
	 * The filter set
	 */
	'filters' => array(
		'id',
		'word' => array(
		    'title' => 'Word',
		),
		'language' => array(
			'title' => 'language',
		)
	),

	/**
	 * The editable fields
	 */
	'edit_fields' => array(
		'word' => array(
			'type' => 'text',
		    'title' => 'Word',
		),
		'language' => array(
			'title' => 'Language',
			'type' => 'text',
		)
	),

	'rules' => array(
    	'word' => 'required',
    	'language' => 'required'
	),
/*
	'action_permissions'=> array(
			'create' => false,
			'delete'=>false,
			),*/

);