<?php

/**
 * Actors model config
 */

return array(

	'title' => 'Scheduler',

	'single' => 'Scheduler',

	'model' => 'Scheduler',

	/**
	 * The display columns
	 */
	'columns' => array(
		'id',
		'description' => array(
		    'title' => 'Description',
			'sort_field' => 'description',
		),
		'time' => array(
			'title' => 'Time',
			'sort_field' => 'time',
			'type' => 'time',
		    'time_format' => 'HH:mm', //optional, will default to this value
		)
	),

	/**
	 * The filter set
	 */
	'filters' => array(
		'id',
		'description' => array(
		    'title' => 'Description',
		),
		'time' => array(
			'title' => 'Time',
			'type' => 'time',
		    'time_format' => 'HH:mm', //optional, will default to this value
		)
	),

	/**
	 * The editable fields
	 */
	'edit_fields' => array(
		'description' => array(
			'type' => 'text',
		    'title' => 'Description',
		),
		'time' => array(
			'title' => 'Time',
			'type' => 'time',
		    'time_format' => 'HH:mm', //optional, will default to this value
		)
	),

	'rules' => array(
    	'description' => 'required',
    	'time' => 'required'
	),
/*
	'action_permissions'=> array(
			'create' => false,
			'delete'=>false,
			),*/

);