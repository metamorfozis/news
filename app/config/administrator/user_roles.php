<?php


return array(

	'title' => 'User Roles',

	'single' => 'User Role',

	'model' => 'AssignedRole',

	/**
	 * The display columns
	 */
	'columns' => array(
		'id',
		'user' => array(
		    'title' => 'User',
		    'relationship' => 'user',
		    'select' => "(:table).email"
		),
		'assignedrole' => array(
		    'title' => 'Role',
		    'relationship' => 'role',
		    'select' => "(:table).name"
		)
	),

	/**
	 * The filter set
	 */
	'filters' => array(
		'id',
		'user_id' => array(
			'title' => 'user_id',
		)
	),

	/**
	 * The editable fields
	 */
	'edit_fields' => array(
		'user' => array(
			'title' => 'user',
			'type' => 'text',
		),
		'assign_role' => array(
		    'setter' => true,
		    'title' => 'Role',
		    'type' => 'enum',
		    'options' => Role::orderBy('name', 'asc')->lists('name', 'id')
		),
	),

);