<?php

/**
 * Actors model config
 */

return array(

	'title' => 'Api Logs',

	'single' => 'Api Logs',

	'model' => 'ApiLogs',

	/**
	 * The display columns
	 */
	'columns' => array(
		'id',
		'api_key_id' => array(
		    'title' => 'Api Key Id',
			'sort_field' => 'api_key_id',
		),
		'route' => array(
			'title' => 'Route',
			'sort_field' => 'route',
		),
		'method' => array(
			'title' => 'Method',
			'sort_field' => 'method',
		),
		'ip_address' => array(
			'title' => 'Ip Address',
			'sort_field' => 'ip_address',
		)
	),

	/**
	 * The filter set
	 */
	'filters' => array(
		'id',
		'api_key_id' => array(
		    'title' => 'Api Key Id',
		),
		'route' => array(
			'title' => 'Route',
		),
		'method' => array(
			'title' => 'Method',
		),
		'ip_address' => array(
			'title' => 'Ip Address',
		)
	),

	/**
	 * The editable fields
	 */
	'edit_fields' => array(
/*		'user_id' => array(
			'type' => 'integer',
		    'title' => 'User Id',
		),*/
		'api_key_id' => array(
			'title' => 'Key',
			'type' => 'number',
		)

	),

	'action_permissions'=> array(
			'create' => false,
			'delete'=>false,
			),

);