<?php

/**
 * Actors model config
 */

return array(

	'title' => 'Users',

	'single' => 'user',

	'model' => 'User',

	/**
	 * The display columns
	 */
	'columns' => array(
		'id',
		'username' => array(
			'title' => 'username',
			'sort_field' => 'username',
		),
		'email' => array(
			'title' => 'email',
			'sort_field' => 'email',
		),
		'assignedrole' => array(
		    'title' => 'Role',
		    'relationship' => 'assignedrole.role',
		    'select' => "(:table).name"
		),
	),

	/**
	 * The filter set
	 */
	'filters' => array(
		'id',
		'username' => array(
			'title' => 'username',
		),
		'email' => array(
			'title' => 'email',
		),
		'assign_role' => array(
		    'setter' => true,
		    'title' => 'Role',
		    'type' => 'enum',
		    'options' => Role::orderBy('name', 'asc')->lists('name', 'id')
		),
	),

	/**
	 * The editable fields
	 */
	'edit_fields' => array(
		'username' => array(
			'title' => 'username',
			'type' => 'text',
		),
		'email' => array(
			'title' => 'email',
			'type' => 'text',
		),
		'assign_role' => array(
		    'setter' => true,
		    'title' => 'Role',
		    'type' => 'enum',
		    'options' => Role::orderBy('name', 'asc')->lists('name', 'id')
		),
	),

	'action_permissions'=> array(
			'create' => false
	),

);