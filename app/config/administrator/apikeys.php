<?php

/**
 * Actors model config
 */

return array(

	'title' => 'Api Keys',

	'single' => 'Api Keys',

	'model' => 'ApiKeys',

	/**
	 * The display columns
	 */
	'columns' => array(
		'id',
		'user_id' => array(
		    'title' => 'User Id',
			'sort_field' => 'user_id',
		),
		'key' => array(
			'title' => 'Key',
			'sort_field' => 'key',
		),
		'ignore_limits' => array(
			'title' => 'Ignore Limits',
			'sort_field' => 'ignore_limits',
		)
	),

	/**
	 * The filter set
	 */
	'filters' => array(
		'id',
		'user_id' => array(
		    'title' => 'User Id',
		),
		'key' => array(
			'title' => 'Key',
		)
	),

	/**
	 * The editable fields
	 */
	'edit_fields' => array(
/*		'user_id' => array(
			'type' => 'number',
		    'title' => 'User Id',
		),*/
		'key' => array(
			'title' => 'Key',
			'type' => 'text',
		),
		'ignore_limits' => array(
			'title' => 'Ignore Limits',
			'type' => 'bool',
		)

	),

	'action_permissions'=> array(
			'create' => false,
			'delete'=>false,
			),

);