<?php
$scheduler_id = DB::table('news_articles')->max('scheduler_id');

$from = Session::get('from', Carbon\Carbon::now()->startOfMonth()->format('Y-m-d'));
$to = Session::get('to', Carbon\Carbon::now()->format('Y-m-d'));
//dd($scheduler_id);
$r = DB::table('news_articles')
    ->join('sources', 'news_articles.source_id', '=', 'sources.id')
   ->select('source_id', 'name', DB::raw("count(source_id) AS tc"))
   ->where('scheduler_id', '=' ,$scheduler_id)
   ->groupBy('source_id')
   ->groupBy('name')
   ->get();
   //dd($r);

$reasons = Lava::DataTable();

$reasons->addStringColumn('Sources')
        ->addNumberColumn('Count');
/*        ->addRow(array('Check Reviews', 5))
        ->addRow(array('Watch Trailers', 2))
        ->addRow(array('See Actors Other Work', 4))
        ->addRow(array('Settle Argument', 89));*/

foreach($r as $rs){
  $reasons->addRow(array($rs->name, $rs->tc));
}
$piechart = Lava::PieChart('IMDB')
                 ->setOptions(array(
                   'datatable' => $reasons,
                   'title' => 'Last Crawling',
                   'is3D' => true
                  ));
//end last crawl

//begin total news by source

$r = DB::table('news_articles')
   ->join('sources', 'news_articles.source_id', '=', 'sources.id')
   ->select('source_id', 'name', DB::raw("count(*) AS tc"))
   ->where('retrieved_time', '>', $from)
   ->where('retrieved_time', '<', $to)
   ->groupBy('source_id')
   ->groupBy('name')
   ->get();

$finances = Lava::DataTable();

$finances->addStringColumn('Sources')
         ->addNumberColumn('Count');
//         ->addNumberColumn('Expenses')
//         ->setDateTimeFormat('Y')
/*         ->addRow(array('2004',400))
         ->addRow(array('2005', 460))
         ->addRow(array('2006', 660))
         ->addRow(array('2007', 1030));*/
foreach($r as $rs){
  $finances->addRow(array($rs->name, $rs->tc));
}
$columnchart = Lava::ColumnChart('Finances')
                    ->setOptions(array(
                      'datatable' => $finances,
                      'title' => 'Total News by Source',
                      'titleTextStyle' => Lava::TextStyle(array(
                        'color' => '#eb6b2c',
                        'fontSize' => 14
                      ))
  ));

//last week trends begin
/*$sources = array();
$src = Sources::where('source_language', '=',Str::upper(App::getLocale()))->get();
      foreach($src as $s){
        $sources[] = $s->id;
}
$searchTerms = Helpers::getLastTenTrends();
$rs = array();
foreach($searchTerms as $st){
          //dd($st);
              $r = DB::table('news_terms')
                 ->join('news_articles', 'news_terms.news_article_id', '=', 'news_articles.id')
                     ->select(DB::raw("date_trunc('day', retrieved_time) AS day, extract(epoch from date_trunc('day', news_articles.retrieved_time)) as dt, count(*) AS tc"))
                     ->where('term', '=', $st)
                     ->whereIn('news_terms.source_id',  $sources)
                     ->groupBy('day')
                     ->orderBy('day')
                     ->get();
                     $r['q'] = $st;
                     $rs[] = $r;
}*/

?>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<div style="margin-top:30px"></div>
<div class="row">
  <div class="col-md-6">
     {{-- <div id="last-crawl"></div> --}}
    <?php echo Lava::render('PieChart', 'IMDB', 'last-crawl', array('height'=>600, 'width'=>900)); ?>
  </div>
  <div class="col-md-6">
          <div class="row">
            {{ Form::open(array('route' => 'postdashboard', 'id' => 'searchform')) }}
            <div class="form-inline">
                 <div class='row' style="margin-top:10px">
                             <div class="col-md-4">
                              <div class="form-group">
                                  <div class='input-group date' >
                                      <input type='text' name="from" class="form-control" id='datetimepicker1' />
                                  </div>
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                    <div class='input-group date' >
                                        <input type='text' name="to" class="form-control" id='datetimepicker2' />
                                    </div>

                              </div>
                       </div>
                        <div class="col-md-4">
                               <button type="submit" class="btn btn-default" style="margin-left:5px;height:28px;width: 100px; padding: 0px 0px;" type="button">Apply</button>
                        </div>
                  </div>
         
              
             </div><!-- /input-group -->
             {{ Form::close() }}
      </div>
      <div id="total-news"></div>
     <?php echo Lava::render('ColumnChart', 'Finances', 'total-news') ?>
  </div>


</div>
<?php 
//Table details of last crawl

$r = DB::table('news_articles')
    ->join('sources', 'news_articles.source_id', '=', 'sources.id')
    //->select('news_articles.id, sources.source_id', 'sources.name')
    ->where('news_articles.scheduler_id', '=' ,$scheduler_id)
   ->get();

   $r = DB::select(DB::raw('select na.id, s.name, na.url, na.title, na.content, na.media, na.author, na.rss_time, na.retrieved_time from news_articles na join sources s on na.source_id = s.id where scheduler_id = (select max(scheduler_id) from news_articles)'));
   
        
  

?>
<div class="row">
  <div class="col-md-12">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>ID</th>
              <th>Source Name</th>
              <th>Url</th>
              <th>Title</th>
              <th>Content</th>
              <th>Media</th>
              <th>Author</th>
              <th>Time</th>
            </tr>
          </thead>
          <tbody>
            @foreach($r as $p)
              <tr>
                  <td><a href="{{URL::to($p->id)}}">{{$p->id}}</a></td>
                  <td>{{$p->name}}</td>
                  <td><a href="{{$p->url}}">Original Link</a></td>
                  <td>{{$p->title}}</td>
                  <td>{{$p->content}}</td>
                  <td><img src="{{$p->media}}" width="100" height="100"></td>
                  <td>{{$p->author}}</td>
                  <td>{{Helpers::rssTimeOrRetrievedTime($p->rss_time, $p->retrieved_time)}}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
  </div>
</div>
{{-- <div class="row">
  <div class="col-md-12">
    <div id="container" style="height: 400px; min-width: 310px"></div>
</div>
</div> --}}


<script>
$(function () {


//datepicker
        $('#datetimepicker1').datetimepicker({
            format: 'YYYY-MM-DD'
        });
        $('#datetimepicker2').datetimepicker({
            useCurrent: false,
            format: 'YYYY-MM-DD'
             //Important! See issue #1075
        });
        $("#datetimepicker1").on("dp.change", function (e) {
            $('#datetimepicker2').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker2").on("dp.change", function (e) {
            $('#datetimepicker1').data("DateTimePicker").maxDate(e.date);

        });

        <?php
                echo '$("#datetimepicker1").data("DateTimePicker").date(new Date("'. $from .'"));';

                echo '$("#datetimepicker2").data("DateTimePicker").date(new Date("'. $to .'"));';

        ?>

});
</script>
<script>
/*var r = {{json_encode($rs, JSON_PRETTY_PRINT)}};
var base_url = "{{ URL::to('/') }}";
$(function () {
$.getJSON(base_url+'/search?trends=1&sr_tr=1', function (data) {
                // Create the chart

                
                var seriesOptions = [];
                $.each(data, function(i, val){
                    var seriesOneData = [];
                    $.each(val, function(j,vl){
                        console.log(vl);
                        if(typeof vl.day !== 'undefined')
                        seriesOneData.push([moment(vl.day).toDate().getTime(),parseInt(vl.tc)]);
                    });
                    seriesOptions[i] = {
                          name: val.q,
                          data: seriesOneData
                    };
                    
                })
               // sd = seriesOneData;//test

                //console.log(seriesOptions);
                $('#container').highcharts('StockChart', {


                  rangeSelector : {
                    selected : 1
                  },

                  series: seriesOptions,

                });
              });
})*/

</script>