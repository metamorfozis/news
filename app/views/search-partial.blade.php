<div class="row search_container">
  <div class="col-lg-2">
  </div>
    <div class="searcharea" style="display:block;margin-left:auto;margin-right:auto">
      
          <div class="col-lg-8">
            {{ Form::open(array('route' => 'postsearch', 'id' => 'searchform')) }}
            <div class="form-inline centerInput">
              <input type="search" name="searchQuery" id="q" class="form-control searchInput" value="{{Input::has('q') ? Input::get('q'): ''}}" placeholder="">
              <input type="hidden" name="from" id="dp1">
              <input type="hidden" name="to" id="dp2">
         
               <button type="submit" class="btn btn-default searchButton hidden-xs hidden-sm" style="margin-left:5px;height:40px;" type="button">{{trans('messages.search')}}</button>
             </div><!-- /input-group -->
             <div class="centerInput">
              <button type="submit" class="btn btn-default searchButtonSm center-block visible-xs visible-sm" style="" type="button">{{trans('messages.search')}}</button>
            </div>
             {{ Form::close() }}
        </div><!-- /.col-lg-8 -->
     
    </div>
    <div class="col-lg-2">
  </div>
</div><!-- /.row -->
@if(isset($home))
<div class="row search_container">
   <div class="" style="display:block;margin-left:auto;margin-right:auto;text-align: center;">
     <div><a id="addnews" href="#"><span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>  {{trans('messages.save_page_now')}} </a></div>
   </br>
   <div>{{trans('messages.save_page_now2')}}</div>
   </div>
</div>
@endif
<div class="row">
<?php /* $displaySearchWarning should be defined on the blade view before including this section*/ ?>
    @if(isset($displaySearchWarning))
    <div class="container">
      <div class="alert alert-warning alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        {{trans('messages.search_text1')}} <br/><br/><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> {{trans('messages.search_text2')}}
      </div>
    </div>
    @endif
</div>
