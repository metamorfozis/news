
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html>

 <!--<![endif]-->


    <head>
	
        
	
	
		<title>{{trans('messages.welcome')}}</title>

		<meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

       <!-- <link rel="stylesheet" href="css/normalize.css"> -->
	  
       <!-- Latest compiled and minified CSS -->
		<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">-->
        <!--<link rel="stylesheet" type="text/css" media="all" href="http://factchecking.mk/wp-content/themes/NetBeans/style.css" />-->


	    <!-- <link rel="stylesheet" href="css/bootstrap-responsive.css"> -->
        
        <link href='http://fonts.googleapis.com/css?family=PT+Sans+Caption:400,700&subset=cyrillic-ext,cyrillic,latin' rel='stylesheet' type='text/css'>

        <link href='http://fonts.googleapis.com/css?family=Russo+One&subset=latin,cyrillic' rel='stylesheet' type='text/css'>

        <link href='http://fonts.googleapis.com/css?family=Cuprum:400,400italic,700,700italic&subset=latin,cyrillic,latin-ext' rel='stylesheet' type='text/css'>
     	
       <?php echo HTML::style('css/bootstrap.min.css')?>
       <?php echo HTML::style('css/style.css')?>
       <?php echo HTML::style('css/bootstrap-datetimepicker.min.css')?>
       <?php echo HTML::style('css/simple-sidebar.css')?>
       <?php echo HTML::style('css/joyride-2.1.css')?>

        @yield('custom_css')

        <script>
         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
         
          ga('create', 'UA-9112588-38', 'auto');
          ga('send', 'pageview');
         
        </script>
    </head>
    
   
    <body>
	<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
   <div id="wrapper"> <!-- Open wrapper -->
  <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
              <li class="page_item"> <a href="#menu-toggle" class="menu-toggle">{{trans('messages.back')}}</a></li>
                <li class="page_item page-item-2742"><a href="{{URL::to('/')}}">{{trans('messages.home')}}</a></li>
                <li class="page_item page-item-2739"><a href="{{URL::to('/about-us')}}">{{trans('messages.about_app')}}</a></li>
                <li class="page_item page-item-2739"><a href="{{URL::to(trans('messages.about_project_route'))}}">{{trans('messages.about_us')}}</a></li>
                <li class="page_item page-item-2739"><a href="{{URL::to('/api')}}">{{trans('messages.api')}}</a></li>
                <li class="page_item"><a href="#" class="help">{{trans('messages.help')}}</a></li>
            </ul>
            <div id="sidebar-filter">

            </div>
        </div>
        <!-- /#sidebar-wrapper -->
   
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        		@yield('everything')

        </div><!-- Close wrapper -->
   </body>
</html>