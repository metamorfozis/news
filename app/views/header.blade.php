        <div class="container">
            <header>
                <div id="topNav" class="row">
                     
                    <div class="col-md-7 col-sm-7">
                      <ul class="navBorder visible-xs visible-sm">
                      <li class="page_item"> <a href="#menu-toggle" class="menu-toggle">{{trans('messages.menu')}}</a></li>
                    </ul>
                        <ul class="navBorder hidden-xs hidden-sm">
<!--<li class="page_item page-item-2748"><a href="#">Commenting Rules</a></li>-->
<li class="page_item page-item-2742"><a href="{{URL::to('/')}}">{{trans('messages.home')}}</a></li>
<li class="page_item page-item-2739"><a href="{{URL::to('/about-us')}}">{{trans('messages.about_app')}}</a></li>
<li class="page_item page-item-2739"><a href="{{URL::to(trans('messages.about_project_route'))}}">{{trans('messages.about_us')}}</a></li>
<li class="page_item page-item-2739"><a href="{{URL::to('/api')}}">{{trans('messages.api')}}</a></li>
<li class="page_item"><a href="#" class="help">{{trans('messages.help')}}</a></li>
<!--<li class="page_item page-item-2745"><a id="addnews" href="#">{{trans('messages.add_news_article')}}</a></li>-->
 
                        </ul>
                    </div><!--end span3 -->
                    <div class="col-md-5 col-xs-5">
                        <ul class="langNav">
                          <?php $locale = App::getLocale();?>
                        <div class="lang_selector"><a <?php if($locale == 'en') echo 'class="active"'?> href="http://timemachine.truthmeter.mk">EN</a> | <a  <?php if($locale == 'sq') echo 'class="active"'?> href="http://makinaekohes.vertetmates.mk">SQ</a> | <a  <?php if($locale == 'mk') echo 'class="active"'?>href="http://vremeplov.vistinomer.mk">MK</a></div> 
                        </ul>
                    </div><!--end span3 -->
                    <div id="searchBar" class="col-md-3">
                      <!--<form method="get" id="searchform" action="http://factchecking.mk/">
                <fieldset>
                       <div class="input-append">
                      <input type="text" class="input-medium" name="s" id="s" placeholder="Пребарувајте ..." />
                      <button type="submit" class="btn" >Барај</button>
                      </div>
                </fieldset>
            <input type='hidden' name='lang' value='en' /></form>
              -->
                        </div><!--end #searchBar span3-->
                </div><!--end Row-->
                
                
                <div class="container">
                        <div class="row">
                            <div id="logoContainer2" class="col-md-12 col-sm-12 col-lg-12">
                                <a href="{{URL::to('/')}}" ><img class="hidden-xs" src="/img/{{App::getLocale()}}/logo1.jpg" /><img class="hidden-xs" src="/img/{{App::getLocale()}}/logo2.jpg" /><img class="visible-xs" src="/img/{{App::getLocale()}}/logo2_small.jpg" /><img src="/img/{{App::getLocale()}}/logo3.jpg" /></a>
                            </div><!--end span9 -->
                        </div><!-- end row -->
                </div>
            </header>
        </div><!--end .container div -->