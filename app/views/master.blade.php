@extends('main')
@section('everything')
    @include('header')
    <div class="container">
    		@yield('content')
    </div>
    @include('footer')
@stop