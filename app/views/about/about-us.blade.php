@extends('master')

@section('content')
<div class="row">
	<div class="col-md-12">

		<h3>{{trans('messages.about_app')}}</h3>
		<p>
			{{trans('messages.about_app_text1')}}
		</p>
		
		</br>

		<p>
			{{trans('messages.about_app_text2')}}
		</p>
    </div>
</div>
@stop