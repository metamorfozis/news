@extends('master')

@section('content')
<div class="row">
<div class="col-md-12">
                  <h3>За проектот</h3>
                            
                            <section class="post_content">
                <p><strong>Проект на УСАИД за зајакнување на медиумите во Македонија – Компонента Сервис за проверка на факти од медиумите спроведувана од Метаморфозис</strong></p>
<p>Граѓаните имаат право на објективни вести и информации засновани на факти и професионални новинарски стандарди!</p>
<p>Проектот на УСАИД за зајакнување на медиумите во Македонија – Компонента Сервис за проверка на факти од медиумите, која во времетраење од 30 месеци ја спроведува Метаморфозис, има за цел да им овозможи на граѓаните на Македонија да бараат поголема отчетност од медиумите, како и да им помогне на новинарите во спроведувањето на нивните професионални стандарди, преку обезбедување онлајн-алатки и ресурси за јавна едукација и подигнување на јавната свест. Намената на онлајн Сервисот за проверка на факти од медиумите е зголемување на побарувачката на граѓаните за објективни вести и информации засновани на факти и професионални новинарски стандарди.</p>
<p>Компонентата Сервис за проверка на факти од медиумите ќе ја спроведува фондацијата за интернет и општество Метаморфозис (<a href="http://metamorphosis.org.mk/" onclick="__gaTracker('send', 'event', 'outbound-article', 'http://metamorphosis.org.mk/', 'www.metamorphois.org.mk');" target="_blank">www.metamorphois.org.mk</a>). Очекуваните проектни резултати опфаќаат:</p>
<ul>
<li>Воспоставување на Сервисот за проверка на факти од медиумите, кој ќе обезбеди:
<ul>
<li>Создавање траен јавен запис со кој ќе се документираат низа клучни прашања поврзани со квалитетот и отчетноста на медиумите во Македонија, преку стручни рецензии на написи и анализи објавени на веб-сајтот mediumi.vistinomer.mk.</li>
<li>Отворен простор за граѓаните и професионалните медиумски работници да одржуваат структурирана јавна дискусија за прашањата поврзани со квалитетот на медиумските производи преку Сервисот како онлајн-платформа.</li>
<li>Основа за застапување за отчетност на медиумите.</li>
</ul>
</li>
<li>Зголемување на побарувачката меѓу граѓаните за објективни вести и информации засновани на факти и професионалните новинарски стандарди, како резултат на објавените едукативни содржини за јавна едукација.</li>
<li>Зголемено ниво на јавна свест за стандардите на професионалното новинарство и примената на критичкото размислување како начин за спротивставување на медиумските девијации.</li>
<li>Зголемена свест на медиумските работници за професионалните стандарди содржани во релевантните кодекси, како и за нивната должност да ја бараат и да известуваат за вистината, на транспарентен и отчетен начин.</li>
</ul>
<p>Проектните активности опфаќаат:</p>
<ul>
<li>Постојана продукција на аналитички написи според однапред определена методологија заснована на етичките новинарски кодекси: рецензии од страна на новинари и аналитички написи со цел да се зголеми јавното образование и да се поттикнат дебати за точноста во медиумските производи.</li>
<li>Соработка со медиумите и граѓанските организации во промоцијата на објективно и професионално новинарство засновано на факти.</li>
</ul>
<p>Официјален веб-сајт:</p>
<ul>
<li>на македонски јазик: <a href="http://mediumi.vistinomer.mk/" onclick="__gaTracker('send', 'event', 'outbound-article', 'http://mediumi.vistinomer.mk/', 'mediumi.vistinomer.mk');">mediumi.vistinomer.mk</a>, достапен и преку кратенката <a href="http://proverkanafakti.mk/">proverkanafakti.mk</a>.</li>
<li>на албански јазик: <a href="http://media.vertetmates.mk/" onclick="__gaTracker('send', 'event', 'outbound-article', 'http://media.vertetmates.mk/', 'media.vertetmates.mk');">media.vertetmates.mk</a>, достапен и преку кратенката&nbsp;<a href="http://verifikimiifakteve.mk/" onclick="__gaTracker('send', 'event', 'outbound-article', 'http://verifikimiifakteve.mk/', 'verifikimiifakteve.mk');">verifikimiifakteve.mk</a>.</li>
<li>на англиски јазик, достапен преку доменот: <a href="http://factchecking.mk/" onclick="__gaTracker('send', 'event', 'outbound-article', 'http://factchecking.mk/', 'factchecking.mk');">factchecking.mk</a></li>
</ul>
<p>Блог за содржини: <a href="http://mediumi.kauza.mk/" onclick="__gaTracker('send', 'event', 'outbound-article', 'http://mediumi.kauza.mk/', 'mediumi.kauza.mk');" target="_blank">mediumi.kauza.mk</a></p>
<p>Профили на социјалните медиуми:</p>
<ul>
<li>Твитер:
<ul>
<li>на македонски: <a href="https://twitter.com/proverkanafakti" onclick="__gaTracker('send', 'event', 'outbound-article', 'https://twitter.com/proverkanafakti', '@ProverkaNaFakti');" target="_blank">@ProverkaNaFakti</a></li>
<li>на албански: <a href="https://twitter.com/VerifikoFaktet" onclick="__gaTracker('send', 'event', 'outbound-article', 'https://twitter.com/VerifikoFaktet', '@VerifikoFaktet');" target="_blank">@VerifikoFaktet</a></li>
<li>на англиски: <a href="https://twitter.com/factcheckingmk/" onclick="__gaTracker('send', 'event', 'outbound-article', 'https://twitter.com/factcheckingmk/', '@FactCheckingMk');">@FactCheckingMk</a></li>
</ul>
</li>
<li>Фејсбук:
<ul>
<li>на македонски: <a href="https://www.facebook.com/ProverkaNaFakti" onclick="__gaTracker('send', 'event', 'outbound-article', 'https://www.facebook.com/ProverkaNaFakti', 'fb.me/ProverkaNaFakti');" target="_blank">fb.me/ProverkaNaFakti</a></li>
<li>на албански: <a href="https://www.facebook.com/VerifikimiIFakteve" onclick="__gaTracker('send', 'event', 'outbound-article', 'https://www.facebook.com/VerifikimiIFakteve', 'fb.me/verifikimiifakteve');" target="_blank">fb.me/verifikimiifakteve</a></li>
<li>на англиски: <a href="https://www.facebook.com/factchecking.mk" onclick="__gaTracker('send', 'event', 'outbound-article', 'https://www.facebook.com/factchecking.mk', 'fb.me/factchecking.mk');">fb.me/factchecking.mk</a></li>
</ul>
</li>
<li>Гугл-плус: <a href="https://plus.google.com/109279064482836797800/posts" onclick="__gaTracker('send', 'event', 'outbound-article', 'https://plus.google.com/109279064482836797800/posts', 'СПФМ');">СПФМ</a></li>
</ul>
<p><strong>Проектен тим</strong></p>
<p>Раководител на проектот: <strong>Филип Стојановски</strong><br>
Уредник на содржините на македонски јазик: <strong>Владимир Петрески</strong><br>
Уредник на содржините на албански јазик:&nbsp;<strong>Петрит Сарачини</strong></p>
<p>Административен асистент: <strong>Љиљана Цекиќ-Лазоровска</strong><br>
Веб-администратор: <strong>Филип Нешкоски</strong></p>
<p>Лектор: <strong>Лилјана Петрушевска</strong></p>
<p><em>Напомена:</em> <em><strong>Цитатите</strong></em> во написите на Сервисот за проверка на факти, како и <em><strong>насловите</strong></em> на текстовите што се рецензираат, <strong>се објавуваат во оригинал и без лектура</strong>. Остатокот од написите е, се разбира, лектуриран. Цитатите преземени од написи на други медиуми и насловите на рецензираните написи од медиумите не се лекторираат со цел читателот да може да ги види и воочи&nbsp;во оригинал сосе сите евенутални грешки – и за да добие вистинска слика, како и за да може да ги следи рецензиите, кои понекогаш говорат и за јазичните неправилности во рецензираните написи.</p>
<p><a href="http://www.metamorphosis.org.mk/mk/kontakt" onclick="__gaTracker('send', 'event', 'outbound-article', 'http://www.metamorphosis.org.mk/mk/kontakt', 'Контакт');">Контакт</a></p>
              </section> <!-- end article section -->
                            


          
                        </div>
</div>
@stop