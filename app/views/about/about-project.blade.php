@extends('master')

@section('content')
<div class="row">
<div class="col-md-12">
                  <h3>About Us</h3>
                            
                            <section class="post_content">
                <p><b>USAID Strengthening Media in Macedonia Project – Media Fact-Checking Service Component</b><b> </b><b>implemented by Metamorphosis</b><b></b></p>
<p><em>Citizens are entitled to fact-based, objective and professional news and information!</em></p>
<p>The 30 months <b>USAID Strengthening Media in Macedonia Project – Media Fact-Checking Service Component </b>implemented by Metamorphosis aims to empower Macedonian citizens to hold the media accountable, and to assist journalists in the implementation of their professional standards, by providing online tools and resources for public education and awareness raising. The purpose of the Media Fact-Checking Service Component is to increase the citizen demand for fact-based, objective and professional news and information.</p>
<p>The Media Fact-Checking Service Component is implemented by the <a href="http://www.metamorphois.org.mk" onclick="__gaTracker('send', 'event', 'outbound-article', 'http://www.metamorphois.org.mk', 'Metamorphosis Foundation for Internet and Society');">Metamorphosis Foundation for Internet and Society</a>.</p>
<p>Expected project outcomes include:</p>
<ul>
<li>Establishment of the Media Fact-Checking Service, which will provide:</li>
<li>The creation of a permanent public record documenting key issues related to media quality and accountability in Macedonia, through expert peer reviews and analyses published on the <a href="http://mediumi.vistinomer.mk/en/" onclick="__gaTracker('send', 'event', 'outbound-article', 'http://mediumi.vistinomer.mk/en/', 'mediumi.vistinomer.mk');">mediumi.vistinomer.mk</a> website.</li>
<li>An open space for citizens and media professionals to hold a structured public discussion on issues related to the quality of media products, through the Service as an online platform.</li>
<li>A basis for advocacy for media accountability.</li>
<li>Increased citizen demand for fact-based, objective and professional news and information as a result of the published public education contents.</li>
<li>Increased level of public awareness about standards of professional journalism and application of critical thinking as a way to confront media deviations.</li>
<li>Increased awareness of media professionals about the professional standards enshrined in relevant codices, and their duty to seek and report the truth, in a transparent and accountable manner.</li>
</ul>
<p>Planned project activities include:</p>
<ul>
<li>Constant production of analytical articles according to a set methodology based on journalistic ethical codices: reviews by journalists and analytical articles aimed to increase public education and incite debates about media accuracy.</li>
<li>Cooperation with the media and civil society organizations in promoting fact-based, objective and professional journalism.</li>
</ul>
<p>Official website:</p>
<ul>
<li>In Macedonian language&nbsp; <a href="http://mediumi.vistinomer.mk" onclick="__gaTracker('send', 'event', 'outbound-article', 'http://mediumi.vistinomer.mk', 'mediumi.vistinomer.mk');">mediumi.vistinomer.mk</a>, also accessible via shortcut domain <a href="http://proverkanafakti.mk/" onclick="__gaTracker('send', 'event', 'outbound-article', 'http://proverkanafakti.mk/', 'proverkanafakti.mk');">proverkanafakti.mk</a>.</li>
<li>In Albanian language <a href="http://media.vertetmates.mk" onclick="__gaTracker('send', 'event', 'outbound-article', 'http://media.vertetmates.mk', 'media.vertetmates.mk');">media.vertetmates.mk</a>, also accessible via <a href="http://verifikimiifakteve.mk/" onclick="__gaTracker('send', 'event', 'outbound-article', 'http://verifikimiifakteve.mk/', 'verifikimiifakteve.mk');">verifikimiifakteve.mk</a>.</li>
<li>In English <a href="http://mediumi.vistinomer.mk/en" onclick="__gaTracker('send', 'event', 'outbound-article', 'http://mediumi.vistinomer.mk/en', 'mediumi.vistinomer.mk/en');">mediumi.vistinomer.mk/en</a>, also accessible via <a href="http://factchecking.mk/">factchecking.mk</a>.</li>
</ul>
<p>Social media profiles:</p>
<ul>
<li>Twitter:
<ul>
<li>in Macedonian: <a href="https://twitter.com/proverkanafakti" onclick="__gaTracker('send', 'event', 'outbound-article', 'https://twitter.com/proverkanafakti', '@ProverkaNaFakti');" target="_blank">@ProverkaNaFakti</a></li>
<li>in Albanian: <a href="https://twitter.com/VerifikoFaktet" onclick="__gaTracker('send', 'event', 'outbound-article', 'https://twitter.com/VerifikoFaktet', '@VerifikoFaktet');" target="_blank">@VerifikoFaktet</a></li>
<li>in English: <a href="https://twitter.com/factcheckingmk/" onclick="__gaTracker('send', 'event', 'outbound-article', 'https://twitter.com/factcheckingmk/', '@FactCheckingMk');">@FactCheckingMk</a></li>
</ul>
</li>
<li>Facebook:
<ul>
<li>in Macedonian: <a href="https://www.facebook.com/ProverkaNaFakti" onclick="__gaTracker('send', 'event', 'outbound-article', 'https://www.facebook.com/ProverkaNaFakti', 'fb.me/ProverkaNaFakti');" target="_blank">fb.me/ProverkaNaFakti</a></li>
<li>in Albanian: <a href="https://www.facebook.com/VerifikimiIFakteve" onclick="__gaTracker('send', 'event', 'outbound-article', 'https://www.facebook.com/VerifikimiIFakteve', 'fb.me/verifikimiifakteve');" target="_blank">fb.me/verifikimiifakteve</a></li>
<li>in English: <a href="https://www.facebook.com/factchecking.mk" onclick="__gaTracker('send', 'event', 'outbound-article', 'https://www.facebook.com/factchecking.mk', 'fb.me/factchecking.mk');">fb.me/factchecking.mk</a></li>
</ul>
</li>
<li>Google Plus: <a href="https://plus.google.com/109279064482836797800/posts" onclick="__gaTracker('send', 'event', 'outbound-article', 'https://plus.google.com/109279064482836797800/posts', 'MFCS');">MFCS</a></li>
</ul>
<p>&nbsp;</p>
<p>Blog serving as contents repository: <a href="http://mediumi.kauza.mk" onclick="__gaTracker('send', 'event', 'outbound-article', 'http://mediumi.kauza.mk', 'mediumi.kauza.mk');">mediumi.kauza.mk</a></p>
<p><strong>Project Team<br>
</strong></p>
<p>Project Manager/Chief of party : <strong>Filip Stojanovski<br>
</strong>Macedonian language contents editor:<strong> Vladimir Petreski<br>
</strong>Albanian language contents editor:<strong>&nbsp;Petrit Saracini</strong></p>
<p>Administrative assistant: <strong>Ljiljana Cekic-Lazorovska</strong><br>
Web administrator: <strong>Filip Neshkoski</strong></p>
<p>Macedonian language proofreader: <strong>Liljana Petrushevska</strong></p>
              </section> <!-- end article section -->
                            


          
                        </div>
</div>
@stop