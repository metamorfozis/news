@extends('master')

@section('content')
<div class="row">
<div class="col-md-12">
                  <h3>Për projektin</h3>
                            
                            <section class="post_content">
                <p><b>Projekti i USAID-it për përforcimin e mediumeve në Maqedoni – Komponenta Shërbimi për verifikimin e fakteve nga mediumet i implementuar nga Metamorfozis</b></p>
<p>Qytetarët kanë të drejtë për lajme dhe informata objektive, që janë të bazuara në fakte dhe në standardet profesionale gazetareske.</p>
<p>Projekti i USAID-it për përforcimin e mediumeve në Maqedoni – Komponenta Shërbimi për verifikimin e fakteve nga mediumet, të cilin në afat prej 30 muajsh e implementon fondacioni Metamorfozis, ka për qëllim t’u mundësojë qytetarëve të Maqedonisë të kërkojnë përgjegjësi nga mediumet, si dhe t’u ndihmojë gazetarëve në zbatimin e standardeve të tyre profesionale, duke siguruar vegla onlajn dhe resurse për edukim dhe ngritje të vetëdijes publike. Qëllimi i projektit për Shërbimi për verifikimin e fakteve nga mediumet është rritja e kërkesës së qytetarëve për lajme objektive dhe informata të bazuara në fakte dhe në standardet profesionale gazetareske.</p>
<p>Komponentën Shërbimi për verifikimin e fakteve nga mediumet do ta implementojë fondacioni për internet dhe shoqëri, Metamorfozis (<a href="http://www.metamorphosis.org.mk/" onclick="__gaTracker('send', 'event', 'outbound-article', 'http://www.metamorphosis.org.mk/', 'www.metamorphosis.org.mk');">www.metamorphosis.org.mk</a>). Rezultatet e pritura nga projekti përfshijnë:</p>
<p>• Krijimin e Shërbimit për verifikimin e fakteve, i cili do të sigurojë:<br>
o Krijimin e një regjistri të përhershëm publik me të cilin do të dokumentohen një sërë çështjesh të rëndësishme që kanë të bëjnë me kualitetin dhe llogaridhënien e mediave në Maqedoni, nëpërmjet recensioneve profesionale të artikujve dhe analizave të publikuara në ueb-faqen mediumi.vistinomer.mk.<br>
o Hapësirë publike për qytetarët dhe punëtorët profesionalë mediatikë ku do të mbajnë diskutim të strukturuar publik për çështje që kanë të bëjnë me kualitetin e produkteve mediatike nëpërmjet Shërbimit si platformë onlajn.<br>
o Bazë për përfaqësim për llogaridhënie të mediave.<br>
• Rritje të kërkesës në mesin e qytetarëve për lajme dhe informata objektive të bazuara në fakte dhe në standardet profesionale gazetareske, si rezultat i përmbajtjeve të publikuara edukative për edukim publik.<br>
• Nivel i shtuar i vetëdijes publike për standardet e gazetarisë profesionale dhe zbatimin e mendimit kritik si mënyrë për t’iu kundërvënë devijimeve mediatike.<br>
• Vetëdije e shtuar e punëtorëve mediatikë për standardet profesionale që gjenden në kodet relevante, si dhe për obligimin e tyre që ta kërkojnë të vërtetën dhe të raportojnë për të, në mënyrë transparente dhe llogaridhënëse.</p>
<p>Aktivitetet e projektit përfshijnë:</p>
<p>• Produksion i vazhdueshëm i artikujve analitikë sipas metodologjisë së përcaktuar më parë, të bazuar në kodet etike gazetareske: recensione nga ana e gazetarëve dhe artikuj analitikë, me qëllim që të ngritet edukimi publik dhe të nxiten debate për saktësinë e produkteve mediatike.<br>
• Bashkëpunim me mediat dhe organizatat qytetare në promovimin e gazetarisë objektive dhe profesionale, të bazuar në fakte.</p>
<p>Ueb-faqja zyrtare:</p>
<p>•&nbsp;<a href="http://mediumi.vistinomer.mk/" onclick="__gaTracker('send', 'event', 'outbound-article', 'http://mediumi.vistinomer.mk/', 'mediumi.vistinomer.mk');">mediumi.vistinomer.mk</a>&nbsp;[maqedonisht] dhe<br>
•&nbsp;<a href="http://media.vertetmates.mk/" onclick="__gaTracker('send', 'event', 'outbound-article', 'http://media.vertetmates.mk/', 'media.vertetmates.mk');">media.vertetmates.mk</a>&nbsp;[shqip]</p>
<p>Blogu për përmbajtjet:&nbsp;<a href="http://mediumi.kauza.mk/" onclick="__gaTracker('send', 'event', 'outbound-article', 'http://mediumi.kauza.mk/', 'mediumi.kauza.mk');" target="_blank">mediumi.kauza.mk</a></p>
<p>Profilet në rrjetet sociale:</p>
<p>• Twitter:&nbsp;<a href="https://twitter.com/proverkanafakti" onclick="__gaTracker('send', 'event', 'outbound-article', 'https://twitter.com/proverkanafakti', '@ProverkaNaFakti');" target="_blank">@ProverkaNaFakti</a>&nbsp;dhe&nbsp;<a href="https://twitter.com/VerifikoFaktet" onclick="__gaTracker('send', 'event', 'outbound-article', 'https://twitter.com/VerifikoFaktet', '@VerifikoFaktet');" target="_blank">@VerifikoFaktet</a><br>
• Facebook:&nbsp;<a href="https://www.facebook.com/ProverkaNaFakti" onclick="__gaTracker('send', 'event', 'outbound-article', 'https://www.facebook.com/ProverkaNaFakti', 'fb.me/ProverkaNaFakti');" target="_blank">fb.me/ProverkaNaFakti</a>&nbsp;dhe&nbsp;<a href="https://www.facebook.com/VerifikimiIFakteve" onclick="__gaTracker('send', 'event', 'outbound-article', 'https://www.facebook.com/VerifikimiIFakteve', 'fb.me/verifikimiifakteve');" target="_blank">fb.me/verifikimiifakteve</a></p>
<p><strong>Ekipi i projektit</strong></p>
<p>Udhëheqësi i projektit: <strong>Filip Stojanovski</strong><br>
Redaktor i përmbajtjeve në gjuhën maqedonase: <strong>Vlladimir Petreski</strong><br>
Redaktor i përmbajtjeve në gjuhën shqipe: <b>Petrit Saracini</b></p>
<p>Asistente administrative: <strong>Liljana Cekiq-Llazorovska</strong><br>
Ueb-administrator: <strong>Filip Neshkoski</strong></p>
<p>Lektor: <strong>Liljana Petrushevska</strong></p>
<p><em>Vërejtje:</em> <em><strong>Citimet</strong></em>&nbsp;në artikujt e Shërbimit për verifikimin e fakteve, si dhe <em><strong>titujt&nbsp;</strong></em>e teksteve që recenzohen, <strong>publikohen në origjinal dhe pa u lekturuar</strong>. Pjesa tjetër e artikujve, gjithsesi, lekturohet. Citimet që merren nga artikujt që janë publikuar në mediat tjera dhe titujt e artikujve të recensuar të mediave nuk lekturohen me qëllim që lexuesi të mund t’i shohë dhe t’i vërejë në origjinal me të gjitha gabimet eventuale – dhe që të fitojë një pasqyrë të vërtetë, si dhe të mund t’i ndjekë recensionet, të cilat ndonjëherë flasin edhe për parregullsitë gjuhësore në artikujt e recensuar.</p>
              </section> <!-- end article section -->
                            


          
                        </div>
</div>
@stop