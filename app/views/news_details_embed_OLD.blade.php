@extends('master2')

@section('custom_css')
<style>
body {
    margin-bottom: 0px;
}
.container {
     padding-right: 0px; 
    padding-left: 0px; 
}
</style>
@stop
@section('content')
    <?php
    			$a = NewsArticles::find($id);
          //$older = NewsArticles::where('parent_id', '=', $a->id)->orderBy('retrieved_time', 'desc')->get();
          $max_similarity = 0;
          if(count($a->similar)) {
             $max_similarity = $a->similar()->first()->percentage * 100; //on the relationship it is ordered by percentage desc so it is the max
          }
    ?>
    <div class="resume">
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8">
    <div class="panel panel-default">
      <div class="panel-heading resume-heading">
        <div class="row">
          <div class="col-lg-12">
            <h4><a href="{{URL::to($a->id)}}" target="_blank">{{$a->title}}</a><h4>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="col-xs-12 col-sm-4">
              <figure>
                {{-- <img class="img-circle img-responsive" alt="" src="http://placehold.it/300x300"> --}}
                <div class="nw-imge" style="background-image:url('{{$a->media}}');"></div>
              </figure>              
            </div>

            <div class="col-xs-12 col-sm-8">
              <ul class="list-group">
                <li class="list-group-item"><a href="{{$a->url}}" target="_blank">{{trans('messages.details_original_link')}}</a> [{{$a->source->name}}]</li>
                @if(Helpers::getSettingsCache('show_screenshot')) <li class="list-group-item"><a href="{{asset('images/screenshots/cache/'.$a->id.'.jpg')}}" target="_blank">{{trans('messages.details_screenshot')}}</a></li>@endif
                <li class="list-group-item"><div id="embed_clear"><a id="embed_button" href="javascript:;">Embed</a></div></li>
                <li class="list-group-item">{{trans('messages.details_max_similarity')}} {{$max_similarity}} %</li>
                <li class="list-group-item">{{trans('messages.details_date')}} : {{Helpers::rssTimeOrRetrievedTime($a->rss_time, $a->retrieved_time)}} </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="bs-callout">
       {{--  <h4>{{$a->title}}</h4> --}}
        <p id="nw-main-article">{{Helpers::formatText($a->content)}}</p>

      </div>
    </div>

  </div>
</div>
</div>
@stop