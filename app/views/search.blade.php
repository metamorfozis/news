@extends('master')

@section('content')
<?php $displaySearchWarning = true; ?>
@include('search-partial')

    <?php

    ?>
  <div class="row">
    @include('filter-partial')
    <div class="col-md-10">
      <?php $results=$searchResult->getItems();
            //dd($searchResult['data']);
              if($searchResult->count() == 0){
       ?>
            <p class="no_results">No results!</p>

       <?php }?>
    @foreach ($results as $a)
        <div class="row news_article">
            <div class="media">
                <a href="{{$a->id}}" target="_blank" class="pull-left">
                  <div class="nw-imge media-object" style="background-image:url('{{Helpers::checkImgExistence($a->media, $a->id)}}');"></div>
                </a>
                <div class="media-body">
                  <a href="{{$a->id}}" target="_blank"><h4 class="media-heading">{{$a->title}}</h4></a>
                        {{str_limit($a->short_content, $limit = 300, $end = '...')}}
                          <div class="media-bottom"> <a href="{{$a->url}}">[{{$a->source->name}}]</a> {{Helpers::rssTimeOrRetrievedTime($a->rss_time, $a->retrieved_time)}}
                          </div>
                </div>
                <div class="nw-similarity">
                      <p>{{trans('messages.similarity')}}</p>
                      @foreach($a->similar as $s)
                          <?php if(count($s->article)){ ?>
                               <a href="{{$s->article->id}}" target="_blank"><p>{{'['. $s->article->source->name . '] - '. str_limit($s->article->title, $limit = 20, $end = '...') .$s->percentage * 100 . ' %'}}</p></a>
                          <?php } ?>
                      @endforeach
                </div>
            </div>
         </div>
    @endforeach
    <div class="nw-pagination">
        <?php echo $searchResult->links(); ?>
    </div>
</div>
</div>
<ol id="chooseID">
  <li data-id="h5date" data-button="{{trans('messages.help_pg2_graphical_button')}}">{{trans('messages.help_pg2_search')}}</li>
  <li data-class="h5methodshow" data-button="{{trans('messages.help_pg1_close')}}">{{trans('messages.help_pg2_graphical')}}</li>
</ol>
@stop
@section('customjs')
<script>
$(window).load(function() {
  if(!$.cookie('joyride2')){
      $("#chooseID").joyride({
            autoStart : true,
            modal:true,
            expose: true,
            cookieMonster: true,
            cookieName: 'joyride2',
            cookieDomain: false
      });
  }


  $(".help").on('click', function(){
      //$.removeCookie('joyride');
      $("#chooseID").joyride({modal:true, expose: true});
      $(window).joyride('restart');
  });

});
</script>
  @include('searchjs')

@stop