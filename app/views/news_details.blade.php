@extends('master')

@section('content')
    <?php
    			$a = NewsArticles::find($id);
          $older = NewsArticles::where('parent_id', '=', $a->id)->orderBy('retrieved_time', 'desc')->get();
          $max_similarity = 0;
          if(count($a->similar)) {
             $max_similarity = $a->similar()->first()->percentage * 100; //on the relationship it is ordered by percentage desc so it is the max
          }
    ?>
@include('search-partial')
<div class="row">
    @include('filter-partial')
<div class="col-md-10 col-xs-12 col-sm-12">
<div class="resume">
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading resume-heading">
        <div class="row">
          <div class="col-lg-12">
            <h4><a href="{{URL::to($a->id)}}" target="_blank">{{$a->title}}</a><h4>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="col-xs-12 col-sm-4">
              <figure>
                {{-- <img class="img-circle img-responsive" alt="" src="http://placehold.it/300x300"> --}}
                <div class="nw-imge" style="background-image:url('{{Helpers::checkImgExistence($a->media, $a->id)}}');"></div>
              </figure>              
            </div>

            <div class="col-xs-12 col-sm-8">
              <ul class="list-group">
                <li class="list-group-item"><a href="{{$a->url}}" target="_blank">{{trans('messages.details_original_link')}}</a> [{{$a->source->name}}]</li>
                @if(Helpers::getSettingsCache('show_screenshot')) <li class="list-group-item"><a href="{{asset('images/screenshots/cache/'.$a->id.'.jpg')}}" target="_blank">{{trans('messages.details_screenshot')}}</a></li>@endif
                <li class="list-group-item"><div id="embed_clear"><a id="embed_button" href="javascript:;">Embed</a></div></li>
                <li class="list-group-item">{{trans('messages.details_max_similarity')}} {{$max_similarity}} %</li>
                <li class="list-group-item">{{trans('messages.details_date')}} : {{Helpers::rssTimeOrRetrievedTime($a->rss_time, $a->retrieved_time)}} </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="bs-callout">
       {{--  <h4>{{$a->title}}</h4> --}}
        <p id="nw-main-article">{{Helpers::formatText($a->content)}}</p>

      </div>
    </div>

  </div>
</div>
<!-- History -->
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading resume-heading">
            <div class="row">
               <div class="col-lg-12">
                  <h4 id="h4history"><?php if(count($older)){echo trans('messages.details_history');} else {echo trans('messages.details_no_history');}?><h4>
                </div>
            </div>
          </div>
        </div>
    </div>
</div>
@if(count($older))
<div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="alert alert-warning">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> 
                {{trans('messages.details_history_of_changes_warning')}}
          </div>
      </div>
   
</div>
@else
<div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="alert alert-warning">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> 
                {{trans('messages.help_pg3_similarity')}}
          </div>
      </div>
   
</div>
@endif
@if(count($older))
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading resume-heading">
          <div class="row">
              <div class="col-md-12">
                   <div id="chart_history"></div>

          </div></div></div></div>
    </div>
</div>
@endif
<?php
$countLegend = count($older);
$countIndex = 1;
$historyChartTable = array('Time');
$historyChartTable[] = (string)$a->id; //Add main article to chart
foreach($older as $ol){
  $historyChartTable[]= (string)$ol->id;
}

$historyChartRow = array();
$historyChartRow[] = $historyChartTable;

//Add main article to chart

    $dt = Helpers::rssTimeOrRetrievedTimeDt($a->rss_time, $a->retrieved_time);
    $r = array(array($dt->hour, $dt->minute, $dt->second));
    for($i = 1; $i <= $countLegend +1; $i++){
      array_push($r, null);
    }
    $r[$countIndex] = 100;
    $countIndex++;
    $historyChartRow[] = $r;
//end add main article to chart

?>
@foreach($older as $ol)
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading resume-heading">
        <div class="row">
          <div class="col-lg-12">
            <h4><a href="{{URL::to($ol->id)}}" target="_blank">{{$ol->title}}</a><h4>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="col-xs-12 col-sm-4">
              <figure>
                {{-- <img class="img-circle img-responsive" alt="" src="http://placehold.it/300x300"> --}}
                <div class="nw-imge" style="background-image:url('{{Helpers::checkImgExistence($ol->media, $ol->id)}}');"></div>
              </figure>              
            </div>
            <?php
                $mx = 0;
                if(count($ol->similar)) {
                   $mx = $ol->similar()->first()->percentage * 100; //on the relationship it is ordered by percentage desc so it is the max
                }

                 $dt = Helpers::rssTimeOrRetrievedTimeDt($ol->rss_time, $ol->retrieved_time);
                 $r = array(array($dt->hour, $dt->minute, $dt->second));
                 for($i = 1; $i <= $countLegend +1; $i++){
                    array_push($r, null);
                 }
                 $r[$countIndex] = $mx;
                 $countIndex++;
                 //$historyChartRow[] = array(array($dt->hour, $dt->minute, $dt->second), $mx, $ol->source->name);
                 $historyChartRow[] = $r;
            ?>
            <div class="col-xs-12 col-sm-8">
              <ul class="list-group">
                <li class="list-group-item"><a href="{{$ol->url}}" target="_blank">{{trans('messages.details_original_link')}}</a> [{{$ol->source->name}}]</li>
                @if(Helpers::getSettingsCache('show_screenshot')) <li class="list-group-item"><a href="{{asset('images/screenshots/cache/'.$ol->id.'.jpg')}}" target="_blank">{{trans('messages.details_screenshot')}}</a></li> @endif
                <li class="list-group-item">{{trans('messages.details_max_similarity_with_main')}} {{$mx}} %</li>
                <li class="list-group-item">{{trans('messages.details_date')}} : {{Helpers::rssTimeOrRetrievedTime($ol->rss_time, $ol->retrieved_time)}}</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="bs-callout">
       {{--  <h4>{{$a->title}}</h4> --}}
        <p class="nw-similar-article">{{Helpers::formatText($ol->content)}}</p>
      </div>
    </div>

  </div>
</div>
@endforeach
<!-- End History -->
<!-- Similar -->
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading resume-heading">
            <div class="row">
               <div class="col-lg-12">
                  <h4 id="h4similarity"><?php if(count($a->similar)){echo trans('messages.details_similar');} else {echo trans('messages.details_no_similar');}?><h4>
                </div>
            </div>
          </div>
        </div>
    </div>
</div>

@if(count($a->similar))
<div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="alert alert-warning">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> 
                {{trans('messages.details_history_of_changes_warning')}}
          </div>
      </div>
   
</div>
@else
<div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="alert alert-warning">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> 
                {{trans('messages.help_pg3_history')}}
          </div>
      </div>
   
</div>
@endif

@if(count($a->similar))
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading resume-heading">
          <div class="row">
              <div class="col-md-12">
                  <div id="chart_similarity"></div>

              
          </div></div></div></div>
    </div>
</div>
@endif
<?php
$countLegendSimilarity = 0;
$countIndexSimilarity = 1;
$similarityChartTable = array('Time');
$similarityChartTable[]= (string)$a->source->name; //Add main article to chart
foreach($a->similarall as $ol){
  
  if(count($ol->article)){
    $similarityChartTable[]= (string)$ol->article->source->name;
    $countLegendSimilarity++;
  }
}
$countLegendSimilarity++;
$similarityChartRow = array();
$similarityChartRow[] = $similarityChartTable;

//Add main article to chart

    $dt = Helpers::rssTimeOrRetrievedTimeDt($a->rss_time, $a->retrieved_time);
    $r = array(array($dt->hour, $dt->minute, $dt->second));
    for($i = 1; $i <= $countLegendSimilarity; $i++){
      array_push($r, null);
    }
    $r[$countIndexSimilarity] = 100;
    $countIndexSimilarity++;
    $similarityChartRow[] = $r;
   
//end add main article to chart

?>
@foreach($a->similarall as $ol)
@if(count($ol->article))
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading resume-heading">
        <div class="row">
          <div class="col-lg-12">
            <h4><a href="{{$ol->article->id}}">{{$ol->article->title}}</a><h4>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="col-xs-12 col-sm-4">
              <figure>
                {{-- <img class="img-circle img-responsive" alt="" src="http://placehold.it/300x300"> --}}
                <div class="nw-imge" style="background-image:url('{{Helpers::checkImgExistence($ol->article->media, $ol->article->id)}}');"></div>
              </figure>              
            </div>
            <?php
                $mx = 0;
                if(count($ol->article->similar)) {
                   $mx = $ol->article->similar()->first()->percentage * 100; //on the relationship it is ordered by percentage desc so it is the max
                }

                 $dt = Helpers::rssTimeOrRetrievedTimeDt($ol->article->rss_time, $ol->article->retrieved_time);
                 $r = array(array($dt->hour, $dt->minute, $dt->second));
                 for($i = 1; $i <= $countLegendSimilarity; $i++){
                    array_push($r, null);
                 }
                //dd($countIndexSimilarity, $similarityChartRow);
                 $r[$countIndexSimilarity] = $ol->percentage * 100;
                 $countIndexSimilarity++;
                 //$historyChartRow[] = array(array($dt->hour, $dt->minute, $dt->second), $mx, $ol->source->name);
                 $similarityChartRow [] = $r;
            ?>
            <div class="col-xs-12 col-sm-8">
              <ul class="list-group">
                <li class="list-group-item"><a href="{{$ol->article->url}}" target="_blank">{{trans('messages.details_original_link')}}</a> [{{$ol->article->source->name}}]</li>
                @if(Helpers::getSettingsCache('show_screenshot')) <li class="list-group-item"><a href="{{asset('images/screenshots/cache/'.$ol->article->id.'.jpg')}}" target="_blank">{{trans('messages.details_screenshot')}}</a></li> @endif
                <li class="list-group-item">{{trans('messages.details_max_similarity_with_main')}} {{$ol->percentage * 100;}} %</li>
                <li class="list-group-item">{{trans('messages.details_date')}} : {{Helpers::rssTimeOrRetrievedTime($ol->article->rss_time, $ol->article->retrieved_time)}}</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="bs-callout">
       {{--  <h4>{{$a->title}}</h4> --}}
        <p class="nw-similar-article">{{Helpers::formatText($ol->article->content)}}</p>
      </div>
    </div>

  </div>
</div>
@endif
@endforeach
<!-- End Similar -->
</div><!-- End Resume -->
</div></div>
<ol id="chooseID">
  <li data-id="nw-main-article" data-button="{{trans('messages.help_pg3_similarity_button')}}">{{trans('messages.help_pg3_main_article')}}</li>
  <li data-id="h4history" data-button="{{trans('messages.help_pg3_history_button')}}">{{trans('messages.help_pg3_similarity')}}</li>
  <li data-id="h4similarity" data-button="{{trans('messages.help_pg1_close')}}">{{trans('messages.help_pg3_history')}}</li>
</ol>
@stop
@section('customjs')
<script>
$(window).load(function() {
  if(!$.cookie('joyride3')){
      $("#chooseID").joyride({
            autoStart : true,
            modal:true,
            expose: true,
            cookieMonster: true,
            cookieName: 'joyride3',
            cookieDomain: false
      });
  }


  $(".help").on('click', function(){
      //$.removeCookie('joyride');
      $("#chooseID").joyride({modal:true, expose: true});
      $(window).joyride('restart');
  });

});
</script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script>
/*var a = "{{{$a->content}}}" ;*/
var inside_news_details = true;
$(function () {
    $('.nw-similar-article').each(function(i, obj) {
       var main_article = $('#nw-main-article').text();
       var cmp_article = this.innerHTML;
       var ds = diffString2(cmp_article, main_article);
       this.innerHTML = ds.o;
       
    });

    $('#embed_button').click(function(event){
        $('#embed_clear').empty();
        $('<input/>').attr({ type: 'text', id: 'embed_text', name: 'embed_text', autofocus: 'true' }).appendTo('#embed_clear');
        $('#embed_text').val('<iframe src="{{URL::to($a->id)}}/embed" width="560" height="315"></iframe>');
      });

});
</script>
<script type="text/javascript">

google.charts.load('current', {packages: ['bar', 'corechart']});

</script>
<script type="text/javascript">
      //google.charts.load('current', {'packages':['corechart']});
      <?php if($countLegend > 0) { ?>
      google.charts.setOnLoadCallback(drawChart1);
      function drawChart1() {
      var data = google.visualization.arrayToDataTable(/*[
          ['Time', 'ID1', 'ID2'],
          [ [10, 10, 0], 50 , null],
          [ [10, 50, 0], null, 80]
        ]*/
         <?php echo json_encode($historyChartRow, true); ?>
        );

        var options = {
          title: "{{trans('messages.details_history_of_changes')}}",
          hAxis: {
           title: 'Time of Day',
           format: 'h:mm a',
           viewWindow: {
            min: [0, 0, 0],
            max: [23, 59, 0]
          }
        },
          vAxis: {
          title: 'Max Similarity',
            minValue: 0,
            maxValue: 100,
            format: '#\'%\''
        },
          legend: {position:'top'}/*,
          chartArea: {  width: "65%", height: "70%" }*/
        };
        
        var chart = new google.visualization.ScatterChart(document.getElementById('chart_history'));

        chart.draw(data, options);

      
      }
      <?php } ?>
    </script>

    <script type="text/javascript">
      //google.charts.load('current', {'packages':['corechart']});
      <?php if($countLegendSimilarity > 1) { ?>
      google.charts.setOnLoadCallback(drawChart2);
      function drawChart2() {
      var data = google.visualization.arrayToDataTable(/*[
          ['Time', 'ID1', 'ID2'],
          [ [10, 10, 0], 50 , null],
          [ [10, 50, 0], null, 80]
        ]*/
         <?php echo json_encode($similarityChartRow, true); ?>
        );

        var options = {
          title: "{{trans('messages.details_similarity_of_articles')}}",
          hAxis: {
           title: 'Time of Day',
           format: 'h:mm a',
           viewWindow: {
            min: [0, 0, 0],
            max: [23, 59, 0]
          }
        },
          vAxis: {
          title: 'Similarity',
            minValue: 0,
            maxValue: 100,
            format: '#\'%\''
        },
          legend: {position:'top'},/*
          chartArea: {  width: "65%", height: "70%" }*/
        };
        
        var chart = new google.visualization.ScatterChart(document.getElementById('chart_similarity'));

/*        function selectHandler() {
    var selectedItem = chart.getSelection()[0];
    if (selectedItem) {
      var value = data.getValue(selectedItem.row, selectedItem.column);
      alert('The user selected ' + value);
    }
  }
*/
       // google.visualization.events.addListener(chart, 'click', selectHandler);
        chart.draw(data, options);

       
      }

       <?php } ?>
    </script>
    <script>
       $( window ).resize(function() {
          if (typeof drawChart1 != "undefined")
            drawChart1();

          if (typeof drawChart2 != "undefined")
            drawChart2();
       });
    </script>

     @include('searchjs')
@stop