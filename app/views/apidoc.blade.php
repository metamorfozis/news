
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">

	<title>Class ApiController</title>

	<?php echo HTML::style('docs/resources/style.css')?>

</head>

<body>
<div id="left">
	<div id="menu">
		<div id="groups">
				<h3>Packages</h3>
			<ul>
				<li>
					<a href="{{URL::to('/api')}}">
						API					</a>

						</li>
			</ul>
		</div>

		<hr>
	</div>
</div>

<div id="splitter"></div>

<div id="right">
<div id="rightInner">

<div id="content" class="class">
	<h1>Api Usage</h1>








	<div class="info">
		
		
		
		<b>Package:</b> <a href="package-API.html">API</a><br>
		<br>
	</div>



	<table class="summary methods" id="methods">
	<caption>Methods summary</caption>
	<tr data-order="search" id="_search">

		<td class="attributes"><code>
			 public 

			json
			
			</code>
		</td>

		<td class="name"><div>
		<a class="anchor" href="#_search">#</a>
		<code><a href="source-class-ApiController.html#16-137" title="Go to source code">search</a>( )</code>

		<div class="description short">
			
		</div>

		<div class="description detailed hidden">
			


				<h4>Api</h4>
				<div class="list">
						Use GET method to access this route<br>
				</div>
				<h4>Returns</h4>
				<div class="list">
					json<br>objects
				</div>


				<h4>Author</h4>
				<div class="list">
						Brilant Kasami<br>
				</div>
				<h4>Link</h4>
				<div class="list">
						<a href="http://timemachine.truthmeter.mk/api/v1/search">http://timemachine.truthmeter.mk/api/v1/search</a><br>
				</div>

				<h4>Params</h4>
				<div class="list">
						q (search term)<br>
				</div>

				<h4>Params</h4>
				<div class="list">
						lang ('sq', 'mk', 'en') default en<br>
				</div>

				<h4>Params</h4>
				<div class="list">
						sources OPTIONAL (source_name = id) example Alsat+M=13 @see http://timemachine.truthmeter.mk/api/v1/getSources<br>
				</div>

				<h4>Params</h4>
				<div class="list">
						sm OPTIONAL (when searching multiple keywords add sm=1 to use OR) ommit this to use AND in the query<br>
				</div>

				<h4>Params</h4>
				<div class="list">
						X-Authorization (Api key)<br>
				</div>

				<h4>Example1</h4>
				<div class="list">
					http://timemachine.truthmeter.mk/api/v1/search?X-Authorization=418e701c797a96b7fd7d3c2b3458555d2d2473dd&lang=sq&q=vmro
					<br>
				</div>

				<h4>Example2</h4>
				<div class="list">
					http://timemachine.truthmeter.mk/api/v1/search?X-Authorization=418e701c797a96b7fd7d3c2b3458555d2d2473dd&lang=sq&q=vmro&Alsat+M=13&TV+Koha=12&sm=1
					<br>
				</div>


		</div>
		</div></td>
	</tr>
	<tr data-order="getSources" id="_getSources">

		<td class="attributes"><code>
			 public 

			json
			
			</code>
		</td>

		<td class="name"><div>
		<a class="anchor" href="#_getSources">#</a>
		<code><a href="source-class-ApiController.html#139-158" title="Go to source code">getSources</a>( )</code>

		<div class="description short">
			
		</div>

		<div class="description detailed hidden">
			
				<h4>Api</h4>
				<div class="list">
						Use GET method to access this route<br>
				</div>

				<h4>Author</h4>
				<div class="list">
						Brilant Kasami<br>
				</div>
				<h4>Link</h4>
				<div class="list">
						<a href="http://timemachine.truthmeter.mk/api/v1/getSources">http://timemachine.truthmeter.mk/api/v1/getSources</a><br>
				</div>
				<h4>Params</h4>
				<div class="list">
						lang ('sq', 'mk', 'en') default en<br>
				</div>
				<h4>Params</h4>
				<div class="list">
						X-Authorization (Api key)<br>
				</div>
				<h4>Example</h4>
				<div class="list">
					http://timemachine.truthmeter.mk/api/v1/getSources?X-Authorization=418e701c797a96b7fd7d3c2b3458555d2d2473dd&lang=sq
					<br>
				</div>

				<h4>Returns</h4>
				<div class="list">
					json<br>objects
				</div>


		</div>
		</div></td>
	</tr>
	</table>
</div>

</div>
</div>
<?php echo HTML::script('docs/resources/combined.js') ?>
<?php echo HTML::script('docs/elementlist.js') ?>
</body>
</html>