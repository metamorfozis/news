<div id="filter-partial-append">
<div class="col-md-2" id="filter-partial">
      <div class="row removeMargin">
                          <div class='row'>
                            <h5 id="h5date">{{trans('messages.date_filter')}}</h5>
                      <div class="form-group">
                          <p>{{trans('messages.from')}}: </p>
                          <div class='input-group date' >
                              <input type='text' class="form-control" id='datetimepicker1' />
                          </div>
                      </div>
                  </div>
                  <div class='row'>
                      <div class="form-group">
                         <p>{{trans('messages.to')}}: </p>
                          <div class='input-group date' >
                              <input type='text' class="form-control" id='datetimepicker2' />
                          </div>
                      </div>
                  </div>
      </div>
            <div class="row">
                <h5>{{trans('messages.source')}}</h5>
                  <?php
                        $lang = Str::upper(App::getLocale());
                        $sources = Sources::where('source_language' , '=', $lang)->orderBy('name')->get();
                  ?>
                  <div class="checkbox sources_filter">
                      <label><input type="checkbox" id="source_all" name="sources[]" data-id="all" value="all" {{Input::has('all') ? 'checked': ''}}>All</label>
                </div>
                  @foreach ($sources as $s)
                      <?php $sn = str_replace(' ', '_', $s->name);?>
                      <div class="checkbox sources_filter">
                            <label><input type="checkbox" name="sources[]" data-id="{{$s->id}}" value="{{$s->name}}" {{Input::has($sn) ? 'checked': ''}}>{{$s->name}}</label>
                      </div>
                  @endforeach
               </div>
               <div class="row">
                <h5>{{trans('messages.search_method')}}</h5>
                    <div class="radio">
                      <label><input type="radio" name="searchmethod" value="0" {{Input::has('sm') ? '': 'checked'}}>{{trans('messages.and')}}</label>
                    </div>
                    <div class="radio">
                      <label><input type="radio" id="trnds" name="searchmethod" value="1" {{Input::has('sm') ? 'checked': ''}}>{{trans('messages.or')}}</label>
                    </div>
              </div>

               <div class="row">
                <h5>{{trans('messages.show')}}</h5>
                    <div class="radio">
                      <label><input type="radio" name="optradio" value="0" {{Input::has('trends') ? '': 'checked'}}>{{trans('messages.text')}}</label>
                    </div>
                    <div class="radio">
                      <label><input type="radio" id="trnds" class="h5methodshow" name="optradio" value="1" {{Input::has('trends') ? 'checked': ''}}>{{trans('messages.graphical')}}</label>
                    </div>
              </div>

              
    </div>
</div>