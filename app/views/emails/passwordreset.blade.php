<h1>New Password</h1>

<p>{{ Lang::get('confide::confide.email.password_reset.greetings', array( 'name' => (isset($user['username'])) ? $user['username'] : $user['email'])) }},</p>

<p>Access the following link to create a new password</p>
<a href='{{ URL::to('users/reset_password/'.$token) }}'>
    {{ URL::to('users/reset_password/'.$token)  }}
</a>

<p>{{ Lang::get('confide::confide.email.password_reset.farewell') }}</p>