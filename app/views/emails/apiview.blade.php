<h1>Api Key</h1>

<p>{{ Lang::get('confide::confide.email.account_confirmation.greetings', array('name' => (isset($username)) ? $username : $email)) }},</p>

<p>Here is your API KEY : {{$apikey}}</p>


<p>{{ Lang::get('confide::confide.email.account_confirmation.farewell') }}</p>
