<script type="text/javascript">
var sd = [];
append_optradio = false;
append_searchmethod=false;
    $(function () {

        var trnds = false;
        if($('input[name=optradio]:checked').val() == 1 )
            trnds = true;


        $( "input[name=optradio]:radio" ).on( "click", function(e){
          if(typeof inside_news_details !== "undefined"){
              append_optradio = true;
              return;
          }
                
                if($(this).attr('value') == 1){
                    if(trnds)
                       return;
                    trnds = true;
                    window.location.href=window.location.href+='&trends=1';
                }
                 else
                 {
                    trnds = false;
                    window.location.href=window.location.href = String(document.location.href).replace("&trends=1", "");
                 }
        });

        //search method 'And' 'Or'
        var searchmethod = 0;

        if($('input[name=searchmethod]:checked').val() == 1 )
            searchmethod = 1;

          
          $("input[name=searchmethod]:radio" ).on( "click", function(e){
            //alert($(this).attr('value'));
                if(typeof inside_news_details !== "undefined"){
                  append_searchmethod = true;
                  return;
                }
                  
                if($(this).attr('value') == 0){
                   // trnds = true;
                   if(searchmethod == 1){
                    window.location.href = String(document.location.href).replace("&sm=1", "");
                   }
                    /*else
                    window.location.href = window.location.href+='&sm=0';*/
                }
                 else if ($(this).attr('value') == 1)
                 {
                      if(searchmethod == 1)
                        return;
                     searchmethod = 1;
                    //trnds = false;
                    if(searchmethod == 0)
                      window.location.href = String(document.location.href).replace("&sm=0", "&sm=1");
                    else
                      window.location.href = window.location.href+='&sm=1';
                 }
        });
        //end search method 'And' 'Or'
        $( "#searchform" ).submit(function( event ) {
          var selectedSources = [];
          $('.sources_filter input:checked').each(function() {
              selectedSources.push($(this).attr('value'));
          });

          if(trnds){
            $(this).append('<input type="hidden" name="trnds" value="' + trnds + '">');
          }
          if(searchmethod == 1){
              $(this).append('<input type="hidden" name="srchmethod" value="' + searchmethod + '">');
          }
          $(this).append('<input type="hidden" name="source" value="'+selectedSources+'">');

          if(append_optradio == true){
            //add to hidden field
          }

          if(append_searchmethod == true){
            //add to hidden field
          }
          return true;

       // event.preventDefault();
      });


      //datepicker
        $('#datetimepicker1').datetimepicker({
            format: 'YYYY-MM-DD'
        });
        $('#datetimepicker2').datetimepicker({
            useCurrent: false,
            format: 'YYYY-MM-DD'
             //Important! See issue #1075
        });
        $("#datetimepicker1").on("dp.change", function (e) {
            $('#datetimepicker2').data("DateTimePicker").minDate(e.date);
            $('#dp1').val(e.date.format("YYYY-MM-DD"));
        });
        $("#datetimepicker2").on("dp.change", function (e) {
            $('#datetimepicker1').data("DateTimePicker").maxDate(e.date);
            $('#dp2').val(e.date.format("YYYY-MM-DD"));
        });

        <?php
            if(Input::has('from'))
                echo '$("#datetimepicker1").data("DateTimePicker").date(new Date("'.Input::get('from').'"));';

            if(Input::has('to'))
                echo '$("#datetimepicker2").data("DateTimePicker").date(new Date("'.Input::get('to').'"));';

        ?>


        $("#source_all").click(function(){
            $('input:checkbox').not(this).prop('checked', this.checked);
        });


        //paginator remove default value
        $('.pagination').on('click', 'a', function(e) {
           // e.preventDefault();
           // console.log($(this).attr('href'));
            var q = $("#q").val();

            var selectedSources = '';
            $('.sources_filter input:checked').each(function() {
                //console.log($(this).attr('data-id'));
                selectedSources+='&' + encodeURIComponent(($(this).attr('value'))) + '='+$(this).attr('data-id');
            });
            //console.log(selectedSources);
            /*if(trnds)
              q += "&trends=1";*/
            if(searchmethod == 1)
              q += "&sm=1";

            if($('#datetimepicker1').val() != ""){
                 q += "&from=" + $('#datetimepicker1').val();
            }

            if($('#datetimepicker2').val() != ""){
                 q += "&to=" + $('#datetimepicker2').val();
            }
            $(this).attr('href', $(this).attr('href') +'&q=' + q + selectedSources);
             //console.log($(this).attr('href'));
        });

        var q = "{{{Input::has('q') ? Input::get('q') : ''}}}";
        var i_trends = "{{{Input::has('trends') ? Input::get('trends') : 0}}}";

        
        if(i_trends == 1){
               $.getJSON(window.location.href+'&sr_tr=1', function (data) {
                // Create the chart
              /*  console.log(JSON.stringify(data));
                var d = data;*/
                
                var seriesOptions = [];
                $.each(data, function(i, val){
                    var seriesOneData = [];
                    $.each(val, function(j,vl){
                        console.log(vl);
                        if(typeof vl.day !== 'undefined')
                        seriesOneData.push([moment(vl.day).toDate().getTime(),parseInt(vl.tc)]);
                    });
                    seriesOptions[i] = {
                          name: val.q,
                          data: seriesOneData
                    };
                    
                })
               // sd = seriesOneData;//test

                //console.log(seriesOptions);
                $('#container').highcharts('StockChart', {


                  rangeSelector : {
                    selected : 1
                  },
              /*
                  title : {
                    text : 'AAPL Stock Price'
                  },*/
                  series: seriesOptions,
/*                  series : [{
                    name : q,
                    data : seriesOptions,
                    tooltip: {
                      valueDecimals: 0
                    }
                  }]*/
                });
              });
        }//end trends

    });
</script>