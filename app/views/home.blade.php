@extends('master')

@section('content')
<?php 
$displaySearchWarning = true;
$home = true;
 ?>
  @include('search-partial')
  <div class="container">
    <div class="row">
        <div> {{trans('messages.about_app_text1')}}&nbsp <a href="{{URL::to('/about-us')}}">more</a></div>
    </div>
  </div>
  <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title centerModal" id="myModalLabel">{{trans('messages.add_news_article')}}</h4>
        <div class="alert centerModal">{{trans('messages.add_news_article_explanation')}}</div>
      </div>
      <div class="modal-body">
       		{{ Form::open(array('route' => 'inserturl')) }}
       		  <div id="error"></div>
       		 <div class="form-group centerModal">
       		 	{{Form::label('insert_url', 'URL: ');}}
            <div class="alert">{{trans('messages.example')}} : </br> {{trans('messages.insert_url_example')}} </div>
       			{{ Form::text('insert_url', isset($insert_url) ? $insert_url : '', $attributes = array('placeholder' => 'URL')) }}
       		 </div>

           <div class="form-group centerModal">
                          <p>{{trans('messages.new_article_date')}}: </p>
                              <input type='text' id='datetimepickerModal' name="date" placeholder="YYYY-MM-DD" />
            </div>
       		  <div class="form-group centerModal">
            {{Form::label('captchaimg', 'Captcha:');}}
       			{{ HTML::image(Captcha::img(), 'Captcha image') }}
       		</br></br>
       			{{Form::label('captcha',trans('messages.text_in_the_box'). ': ');}}
       			{{ Form::text('captcha') }}
       		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Add</button>
        {{ Form::close() }}
      </div>
    </div>
  </div>
</div></div></div>

<ol id="chooseID">
  <li data-id="logoContainer2" data-button="{{trans('messages.help_pg1_search_button')}}">{{trans('messages.help_pg1_header')}}</li>
  <li data-class="searchButton" data-button="{{trans('messages.help_pg1_archive_button')}}">{{trans('messages.help_pg1_search')}}</li>
  <li data-id="addnews" data-button="{{trans('messages.help_pg1_close')}}">{{trans('messages.help_pg1_archive')}}</li>
</ol>
@stop

@section('customjs')
<script>
$(window).load(function() {
  if(!$.cookie('joyride')){
      $("#chooseID").joyride({
            autoStart : true,
            modal:true,
            expose: true,
            cookieMonster: true,
            cookieName: 'joyride',
            cookieDomain: false
      });
  }


  $(".help").on('click', function(){
      //$.removeCookie('joyride');
      $("#chooseID").joyride({modal:true, expose: true});
      $(window).joyride('restart');
  });

});
</script>
<script>
     <?php 
	     $a = isset($popupOpen) ? "true"  : "false";

	     echo "popupOpen = " . $a. ";" ;

	    // $f = isset($news_inserted_failure) ? $news_inserted_failure : "Unknown error";
	     if(isset($news_inserted_failure))
	     	echo "news_inserted_failure = '" . $news_inserted_failure. "';" ;
	     else
	     	echo "news_inserted_failure = false;";

     ?>

    
       $(function () {

          $('#datetimepickerModal').datetimepicker({
            format: 'YYYY-MM-DD'
           });

          $("#addnews").on('click', function(e){
              $('#myModal').modal('toggle');
          });
       		if(popupOpen){
       			$('#myModal').modal('toggle');
       		}
       		 if(news_inserted_failure){
       		 		$('#myModal').modal('toggle');
			     	$("#error").append('<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>'
			     		+'<span class="sr-only">Error:</span>'+news_inserted_failure+'</div>');
			     }
       });
</script>
@stop