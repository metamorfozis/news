<footer class="footr">
<div class="container">
      <div class="darkGrey">
          
                <div class="footer">
                	{{trans('messages.part_of1')}} <a href="{{trans('messages.part_of_link')}}">{{trans('messages.part_of2')}}</a>
                    &nbsp;{{trans('messages.footer')}}
                </div>   
                <div class="footer"></div>
    </div>
<?php echo HTML::script('js/jquery.min.js') ?>
<?php echo HTML::script('js/bootstrap.min.js') ?>
<?php echo HTML::script('js/moment.min.js') ?>
<?php echo HTML::script('js/bootstrap-datetimepicker.min.js') ?>
<?php echo HTML::script('js/jsdiff.js') ?>
<?php echo HTML::script('js/highstock.js') ?>
<?php echo HTML::script('js/jquery.cookie.js') ?>
<?php echo HTML::script('js/jquery.joyride-2.1.js') ?>
<!-- Menu Toggle Script -->
    <script>
    $(".menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

detached = false;
if($( document ).width() < 992){
    p = $("#filter-partial").detach();
                //p = .attach();
                p.appendTo("#sidebar-filter");
                detached = true;
}
     $( window ).resize(function() {
            if($( document ).width() < 992)
        	{
        		p = $("#filter-partial").detach();
        		//p = .attach();
        		p.appendTo("#sidebar-filter");
        		detached = true;
        		
        	}
        	else{
        			if(detached ==true){
        				p = $("#filter-partial").detach();
        				p.appendTo("#filter-partial-append");
        				detached = false;
        			}
                    $("#wrapper").toggleClass("toggled", false);
        	}
	});
</script>
@yield('customjs')
</div></footer> 